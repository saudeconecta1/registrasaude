require('react-native').unstable_enableLogBox();

import { AppRegistry } from 'react-native';
import AppContainer from "./src/navigation";
import { name as appName } from './app.json';

AppRegistry.registerComponent(appName, () => AppContainer);
