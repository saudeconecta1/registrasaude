import React from "react";
import { StyleSheet, TextInput, View } from "react-native";

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const FormInput = ({
  iconName,
  iconColor,
  returnKeyType,
  keyboardType,
  name,
  placeholder,
  rightIcon,
  minhaRef,
  ...rest
}) => (
    <View style={styles.containerInput}>
      <Icon
        style={styles.iconStyle}
        name={iconName}
        size={22}
        color={iconColor} />
      <TextInput
        {...rest}
        style={styles.input}
        ref={minhaRef}
        name={name}
        keyboardType={keyboardType}
        autoCompleteType="off"
        placeholder={placeholder}
        placeholderTextColor="#545454"
        returnKeyType={returnKeyType}
      />
      {rightIcon && rightIcon}
    </View>
  );

const styles = StyleSheet.create({
  containerInput: {
    marginHorizontal: 50,
    borderWidth: 1,
    borderColor: '#545454',
    borderRadius: 25,
    flexDirection: "row",
    alignItems: "center",
  },
  input: {
    flex: 1,
    marginRight: 5,
    fontFamily: 'Roboto-Regular',
    fontSize: 14,
    color: "#545454",
  },
  iconStyle: {
    marginLeft: 20,
    marginRight: 10
  }
});

export default FormInput;