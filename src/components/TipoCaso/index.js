import React, { useState, useContext } from 'react';
import { View, Text } from 'react-native';

import styles from './styles';

export default function TipoCaso({ caso }) {

  if (caso === 1) {
    return (
      <>
        <Text style={styles.textoTipoCaso}>CASO SUSPEITO</Text>
        <Text style={styles.textoOrientacao}>
          Notifique o caso e oriente quanto ao isolamento domiciliar.
        </Text>
      </>
    )
  } else if (caso === 2) {
    return (
      <>
        <Text style={styles.textoTipoCaso}>ACOMPANHE DE PERTO ESSA PESSOA!</Text>
        <Text style={styles.textoOrientacao}>
          Realize um novo contato com essa pessoa em até uma semana.
        </Text>
      </>
    )
  } else {
    return (
      <>
        <Text style={styles.textoTipoCaso}>Obrigado por colaborar!</Text>
        <Text style={styles.textoOrientacao}>
          Realize um novo contato com essa pessoa em até uma semana.
      </Text>
      </>
    )
  }
}
