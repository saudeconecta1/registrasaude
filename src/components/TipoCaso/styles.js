import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    textoTipoCaso: {
        fontFamily: 'RobotoCondensed-Bold',
        fontSize: 18,
        color: "#4E4E4E",
    },
    textoOrientacao: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: "#4E4E4E",
        textAlign: "center",
        width: 280,
    },
});