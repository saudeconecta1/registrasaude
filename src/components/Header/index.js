import React from 'react';
import { Image, View, Text } from 'react-native';

import styles from './styles';

import scImg from '../../assets/saudeconecta.png';

export default function Header() {
    return (
        <>
            <View style={styles.header}>
                <View style={styles.containerHeader}>
                    <Text style={styles.textoHeader}>
                        Condições de Saúde
                    </Text>
                </View>

                <Image source={scImg} style={styles.scImg} />
            </View>
        </>
    )
}
