import { StyleSheet } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';

export default StyleSheet.create({
    header: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        backgroundColor: "#FFF",
        overflow: "hidden",
        height: 150,
    },
    containerHeader: {
        width: 150,
        marginLeft: 40,
        marginTop: 30,
    },
    textoHeader: {
        fontFamily: 'RobotoCondensed-Bold',
        fontSize: 20,
        color: "#00B8AB",
    },
    textoCovid: {
        fontFamily: 'Roboto-Regular',
        fontSize: 20,
        color: "#545454",
    },
    scImg: {
        width: 86,
        height: 116,
        marginTop: 10,
        marginRight: 40,
    },
});