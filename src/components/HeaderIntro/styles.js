import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    header: {
        height: 160,
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: 'center',
    },
    scLogo: {
        marginTop: -40,
        marginLeft: -70,
    },
    logoImg: {
        width: 116,
        height: 50,
    },
});