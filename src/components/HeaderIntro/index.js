import React from 'react';
import { Image, View } from 'react-native';

import styles from './styles';

import scLogo from '../../assets/saudeconecta.png';
import logoImg from '../../assets/logo.png';

export default function HeaderIntro() {
    return (
        <>
            <View style={styles.header}>
                <Image source={scLogo} style={styles.scLogo} />
                <Image source={logoImg} style={styles.logoImg} />
            </View>
        </>
    )
}
