import React from "react";
import { Button } from "react-native-elements";

const FormButton = ({ title, buttonType, buttonColor, ...rest }) => (
  <Button
    {...rest}
    type={buttonType}
    title={title}
    buttonStyle={
      {
        marginHorizontal: 40,
        backgroundColor: buttonColor,
        borderRadius: 25
      }
    }
    titleStyle={
      {
        fontFamily: 'Roboto-Medium',
        fontSize: 14,
        color: "#FFF",
      }
    }
  />
);

export default FormButton;