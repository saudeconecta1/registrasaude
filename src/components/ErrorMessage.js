import React from "react";
import { View, Text, StyleSheet } from "react-native";

const ErrorMessage = ({ errorValue }) => (
  <View style={styles.container}>
    <Text style={styles.errorText}>{errorValue}</Text>
  </View>
);

const styles = StyleSheet.create({
  container: {
    marginLeft: 75
  },
  errorText: {
    fontFamily: 'Roboto-Regular',
    fontSize: 12,
    color: "red"
  }
});

export default ErrorMessage;
