import React from 'react';
import { Text } from 'react-native';

import styles from './styles';

export default function TipoCasoEscala({ caso }) {
  if (caso === 1) {
    return (
      <>
        <Text style={styles.textoTipoCaso}>VULNERABILIDADE HABITUAL</Text>
      </>
    )
  } else if (caso === 2) {
    return (
      <>
        <Text style={styles.textoTipoCaso}>VULNERABILIDADE MENOR</Text>
      </>
    )
  } else if (caso === 3) {
    return (
      <>
        <Text style={styles.textoTipoCaso}>VULNERABILIDADE MÉDIA</Text>
      </>
    )
  } else {
    return (
      <>
        <Text style={styles.textoTipoCaso}>VULNERABILIDADE MÁXIMA</Text>
      </>
    )
  }
}
