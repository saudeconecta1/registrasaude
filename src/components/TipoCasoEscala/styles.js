import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    textoTipoCaso: {
        fontFamily: 'RobotoCondensed-Bold',
        fontSize: 18,
        color: "#4E4E4E",
    },
});