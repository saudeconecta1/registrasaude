import React, { useState, useContext } from 'react';
import { View, Text, TouchableOpacity, Button } from 'react-native';

import AntDesign from 'react-native-vector-icons/AntDesign';

import styles from './styles';
import { Colors } from '../../Colors';

export default function Accordion(props) {
    const [data, setData] = useState(props.data);
    const [expanded, setExpanded] = useState(false);

    function toggleExpand() {
        setExpanded(!expanded)
    }

    return (
        <>
            <TouchableOpacity
                style={[styles.row, { backgroundColor: props.color }]}
                onPress={toggleExpand}>
                <Text style={[styles.title, styles.font]}>
                    {props.title}
                </Text>

                <AntDesign
                    name={expanded ? 'caretup' : 'caretdown'}
                    size={16} color={Colors.WHITE}
                />
            </TouchableOpacity>

            <View style={styles.parentHr} />
            {
                expanded &&
                <View style={styles.child}>
                    {data}
                </View>
            }
        </>
    )
}
