import { StyleSheet } from 'react-native';
import { Colors } from '../../Colors';

export default StyleSheet.create({
    row: {
        backgroundColor: "#CCC",
        borderColor: '#545454',
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        marginVertical: 15,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingVertical: 15,
        paddingLeft:60,
        paddingRight: 20,
        marginRight: 45
    },
    title: {
        fontSize: 18,
        fontWeight: "bold",
        color: "#FFF",
    },
    parentHr: {
        height: 1,
        color: Colors.WHITE,
        width: '100%'
    },
    child: {
        paddingHorizontal: 40,
        paddingVertical: 20
    }
});