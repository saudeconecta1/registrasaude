import AsyncStorage from '@react-native-community/async-storage';
import { CommonActions } from '@react-navigation/native';

export async function getUser() {
  try {
    return await AsyncStorage.getItem('@ListApp:userToken');
  } catch (e) {
    throw e;
  }
}

export async function storeUser(userToken) {
  try {
    return await AsyncStorage.setItem('@ListApp:userToken', JSON.stringify(userToken));
  } catch (e) {
    throw e;
  }
}

export async function deleteUser() {
  try {
    return await AsyncStorage.removeItem('@ListApp:userToken');
  } catch (e) {
    throw e;
  }
}

export async function salvarAviso(aviso) {
  try {
    return await AsyncStorage.setItem('@ListApp:aviso', JSON.stringify(aviso));
  } catch (e) {
    throw e;
  }
}

export async function getAviso() {
  try {
    return await AsyncStorage.getItem('@ListApp:aviso');
  } catch (e) {
    throw e;
  }
}

// NavigationService

let navigator;

export function setTopLevelNavigator(navigatorRef) {
  navigator = navigatorRef;
}

export function navigate(routeName, params) {
  navigator.dispatch(
    CommonActions.navigate({
      routeName,
      params,
    }),
  );
}