import { StyleSheet } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';

export default StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20,
        marginHorizontal: 20,
        marginVertical: 20,
        borderRadius: 10,
    },
    containerTexto: {
        flex: 1,
        paddingHorizontal: 30,
        paddingVertical: 30,
        backgroundColor: "#FFF",
        borderRadius: 10,
        justifyContent: "space-between"
    },
    texto: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: "#545454",
        marginBottom: 25,
    },
    btnComecar: {
        height: 48,
        backgroundColor: "#1C72BA",
        alignItems: "center",
        justifyContent: "center",
        borderWidth: 0.5,
        borderColor: '#545454',
        borderRadius: 50,
    },
    comecar: {
        color: "#FFF",
        fontSize: 14
    },
});