import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';

import styles from './styles';
import { getUser, salvarAviso } from '../../../src/utils';

import CovidAppContext from '../../contexts/CovidAppContext';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default function Onboarding() {
    const navigation = useNavigation();
    const { setHasAvisoContext } = React.useContext(CovidAppContext);

    async function handleComecar() {
        const dataUser = await getUser();
        const { user } = JSON.parse(dataUser);

        const aviso = { idUser: user.id, aviso: false };
        await salvarAviso(aviso);
        setHasAvisoContext(false);
        navigation.navigate('Home');
    }

    return (
        <View style={styles.container}>
            <View style={styles.containerTexto}>
                <View>
                    <Text style={styles.texto}>
                        Este aplicativo monitora onde estão os casos suspeitos de COVID-19.
                    </Text>
                    <Text style={styles.texto}>
                        As informações coletadas geram relatórios importantes para a Atenção à Saúde na sua região.
                    </Text>
                    <Text style={styles.texto}>
                        O aplicativo não está programado para sincronizar os dados automaticamente.
                    </Text>
                    <Text style={styles.texto}>
                        Para ter informação mais precisa nos relatórios, é preciso que você faça a sincronização a cada
                        48 horas, no botão <Icon name={'sync'} size={24} color={'#545454'} />
                        na barra inferior do seu aplicativo.
                    </Text>
                    <Text style={styles.texto}>
                        Seus registros serão mais um força para conter a COVID-19!
                    </Text>
                </View>
                <View>
                    <TouchableOpacity
                        style={styles.btnComecar}
                        onPress={handleComecar}
                    >
                        <Text style={styles.comecar}>Começar</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </View >
    )
}