import React, { useState, useEffect, useCallback } from 'react';
import { ActivityIndicator, Alert, Image, View, Text, TouchableOpacity } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import * as Progress from 'react-native-progress';
import { RelatorioService } from '../../api';
import Database from '../../banco/Database';

import { getUser } from '../../../src/utils';
import { differenceInHours, format } from 'date-fns';
import { pt } from 'date-fns/locale'

import styles from './styles';
import Header from '../../components/Header';
import IconAlerta from '../../assets/alerta.png';

import CovidAppContext from '../../contexts/CovidAppContext';

function Sincronizacao() {
    const navigation = useNavigation();
    const db = new Database();
    const relatorioService = new RelatorioService();
    const [isNeedSync, setIsNeedSync] = useState(false);
    const [haveRecords, setHaveRecords] = useState(false);
    const [sync, setSync] = useState(false);
    const [dateSync, setDateSync] = useState('Não sincronizado');
    const [userLogado, setUserLogado] = useState(null);
    const [progress, setProgress] = useState(0);
    const [indeterminate, setIndeterminate] = useState(true);
    const { hasSyncContext, setHasSyncContext } = React.useContext(CovidAppContext);

    useEffect(() => {
        loadData();
    }, []);

    useEffect(() => () => console.log("unmount sincronizacao"), []);

    const loadData = useCallback(async () => {
        try {
            const dataUser = await getUser();
            const { user } = JSON.parse(dataUser);
            setUserLogado(user);

            const dataInfo = await db.getDateSincronia(user.id);
            console.log("::GET QTD REGISTRO E SINCRONIA:", dataInfo);

            if (dataInfo) {
                getDtSincronismo(dataInfo);
            } else {
                const qtdNotSinc = await db.getQtdRegistros(user.id);
                console.log("::GET QUANTIDADE DE REGISTROS NÃO SINCRONIZADOS:", qtdNotSinc);
                setHaveRecords(qtdNotSinc > 0);
            }

        } catch (err) {
            console.log(err);
        }
    });

    function getDtSincronismo(info) {
        const { sincroniaDate, qtd } = info;
        const temRegistro = qtd > 0;
        const dtSincronismo = Number(sincroniaDate);
        const dtAtual = Date.now();
        const diff = differenceInHours(dtAtual, dtSincronismo);
        const formattedDate = format(
            dtSincronismo,
            "dd/MM/yyyy ' às ' HH:mm:ss",
            { locale: pt }
        );

        setDateSync(formattedDate);

        if (diff >= 48 && temRegistro) {
            setIsNeedSync(true);
        } else {
            setIsNeedSync(false);
        }

        setHaveRecords(temRegistro);
        setHasSyncContext(temRegistro);

        console.log(`# Tempo após a última sincronizar: ${diff} horas`);
        console.log('# Necessário sincronizar: ', temRegistro);
        console.log('# Número de Registros para sincronizar: ', qtd);
    }

    function animate() {
        let progress = 0;
        setProgress(progress);

        setTimeout(() => {
            setIndeterminate(false);

            setInterval(() => {
                progress += Math.random() / 5;
                if (progress > 1) {
                    progress = 1;
                }
                setProgress(progress);
            }, 500);
        }, 1500);
    }

    async function sincronizar() {
        setSync(true);
        try {
            const relatorios = await db.listarRelatorioNaoSincronizados(userLogado.id);

            if (!relatorios.length) {
                return false;
            }

            setIsNeedSync(false);

            const idUser = userLogado.id;
            const insertDate = new Date();
            const data = prepararForm(relatorios);
            const resultRelatorio = await relatorioService.enviarRelatorio(data);

            console.log('total de cadastros no servidor: ', resultRelatorio.data._total);
            //atualiza os dados sincronizados
            await db.atualizarSincronizados(resultRelatorio.data._ids, idUser);

            //insere a data de sincronia
            await db.adicionarSincronia(idUser, insertDate.getTime());

            //animate();

            //Atribui a nova data de 
            const formattedDate = format(
                insertDate,
                "dd/MM/yyyy ' às ' HH:mm:ss",
                { locale: pt }
            );

            setDateSync(formattedDate);
            setHaveRecords(false);
            setHasSyncContext(false);
            setSync(false);

            const msgSucesso = relatorios.length > 1
                ? `Foram sincronizados ${relatorios.length} registros!`
                : `Foi sincronizado ${relatorios.length} registro!`;

            Alert.alert('Aviso', msgSucesso,
                [{ text: 'OK' }],
                { cancelable: false },
            );
        } catch (e) {
            console.log('::Erro ao enviar os relatorios', e);
            Alert.alert('Aviso', 'Erro ao sincronizar os relatorios',
                [{ text: 'OK' }],
                { cancelable: false },
            );
            setSync(false);
        }
    }

    function prepararForm(relatorios) {
        const formData = new FormData();

        relatorios.forEach((element, key) => {
            //console.log("Sincronizar Registro: ", element);

            let latitude = element.relatorio.latitude
                ? element.relatorio.latitude : 0;
            let longitude = element.relatorio.longitude
                ? element.relatorio.longitude : 0;

            formData.append(`relatorio[${key}][row]`, element.id);
            formData.append(`relatorio[${key}][cpf]`, element.relatorio.cpfVisita);
            formData.append(`relatorio[${key}][teve_febre]`, element.relatorio.teveFebre);
            formData.append(`relatorio[${key}][teve_tosse]`, element.relatorio.teveTosse);
            formData.append(`relatorio[${key}][teve_dor_garganta]`, element.relatorio.teveDorGarganta);
            formData.append(`relatorio[${key}][teve_dificuldade_respirar]`, element.relatorio.teveDificuldadeRespirar);
            formData.append(`relatorio[${key}][teve_contato]`, element.relatorio.teveContato);
            formData.append(`relatorio[${key}][anotacao]`, element.relatorio.anotacao);
            formData.append(`relatorio[${key}][logradouro]`, element.relatorio.logradouro);
            formData.append(`relatorio[${key}][latitude]`, latitude);
            formData.append(`relatorio[${key}][longitude]`, longitude);
            formData.append(`relatorio[${key}][user_id]`, element.idUser);
            formData.append(`relatorio[${key}][tipo_caso]`, element.tipoCaso);

            console.log("Sincronizar Registro: ", formData);
        });

        return formData;
    }

    function handleProbabilidade() {
        navigation.navigate("Probabilidade");
    }

    return (
        <View style={styles.container}>
            <Header />

            {sync &&
                <View style={styles.circles}>
                    <Progress.Pie
                        size={80}
                        indeterminate={true}
                        useNativeDriver={true}
                        color={'#1C72BA'}
                    />
                </View>
            }

            <View style={styles.containerStatus}>
                <Text style={styles.textoStatus}>ÚLTIMA SINCRONIZAÇÃO</Text>
            </View>

            <View style={[styles.containerHorario,
            isNeedSync ? { backgroundColor: "#F51222" }
                : { backgroundColor: "#00B8AB" }
            ]}>
                <Text style={styles.textoHorario}>
                    {dateSync}
                </Text>
            </View>

            {isNeedSync &&
                <View style={styles.containerAviso}>
                    <Image source={IconAlerta} style={styles.image} />

                    <View style={styles.containerAviso}>
                        <Text style={styles.textoAviso}>
                            Faz mais de dois dias que não registramos sincronização.
                        </Text>
                    </View>
                </View>
            }

            <View style={styles.containerBtn}>
                {haveRecords &&
                    <TouchableOpacity
                        style={styles.btnSincronizar}
                        onPress={sincronizar}>
                        {sync ? (
                            <ActivityIndicator size="small" color="#FFF" />
                        ) : (
                                <Text style={styles.textoBtn}>Sincronizar</Text>
                            )}
                    </TouchableOpacity>
                }

                <TouchableOpacity
                    style={styles.btnVerDados}
                    onPress={handleProbabilidade}>
                    <Text style={styles.textoBtnVer}>Ver meu território</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default Sincronizacao;
