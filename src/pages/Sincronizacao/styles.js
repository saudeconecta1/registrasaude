import { StyleSheet } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFF",
    },
    containerAviso: {
        alignItems: "center",
        marginTop: 10,
    },
    textoAviso: {
        width: 280,
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: "#4E4E4E",
        textAlign: "center"
    },
    containerStatus: {
        marginTop: 40,
        marginBottom: 20,
        alignItems: "center"
    },
    textoStatus: {
        fontFamily: 'RobotoCondensed-Bold',
        fontSize: 18,
        color: "#4E4E4E",
    },
    containerHorario: {
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        marginVertical: 15,
        alignItems: "center",
        paddingVertical: 10,
        paddingLeft: 60,
        paddingRight: 20,
        marginRight: 35
    },
    textoHorario: {
        fontFamily: 'RobotoCondensed-Bold',
        fontSize: 18,
        color: "#FFF",
    },
    containerBtn: {
        flex: 1,
        justifyContent: "center",
        margin: 25,
    },
    btnSincronizar: {
        backgroundColor: "#1C72BA",
        paddingVertical: 10,
        paddingHorizontal: 30,
        marginHorizontal: 40,
        borderRadius: 25,
        justifyContent: "center",
        alignItems: "center",
    },
    btnVerDados: {
        backgroundColor: "#FFF",
        borderWidth: 1,
        borderColor: '#1C72BA',
        paddingVertical: 10,
        paddingHorizontal: 30,
        marginVertical: 40,
        marginHorizontal: 40,
        borderRadius: 25,
        justifyContent: "center",
        alignItems: "center",
    },
    textoBtn: {
        fontFamily: 'Roboto-Medium',
        fontSize: 14,
        color: "#FFF",
    },
    textoBtnVer: {
        fontFamily: 'Roboto-Medium',
        fontSize: 14,
        color: "#1C72BA",
    },
    circles: {
        alignItems: 'center',
    }
});