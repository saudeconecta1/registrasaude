import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFF",
    },
    containerFavorito: {
        width: 50,
        justifyContent: "flex-start",
        alignItems: "center",
    },
    containerAcoes: {
        width: 50,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "flex-start",
    },
    btnLista: {
        alignItems: 'center',
        marginRight: 10,
        padding: 5,
    },
    textoNotas: {
        fontSize: 14,
        marginBottom: 10,
    },
    containerInputNota: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        marginHorizontal: 20,
        marginVertical: 10,
    },
    inputTextNotas: {
        flex: 1,
        alignItems: "center",
        marginHorizontal: 10,
        paddingHorizontal: 10,
        borderWidth: 2,
        borderColor: '#DBDBDB',
        borderRadius: 10,
        backgroundColor: "#FFFFFF",
    },
    containerTexto: {
        flex: 1,
        justifyContent: "center",
        paddingVertical: 5,
        paddingHorizontal: 5,
        marginRight: 15,
    },
    inputTextLista: {
        flex: 1,
        borderBottomWidth: 1,
        borderColor: '#ccc',
        paddingHorizontal: 5,
        paddingVertical: 0,
        marginVertical: 0,
        marginRight: 15,
    },
    btnNew: {
        width: 24,
        height: 24,
        borderRadius: 24 / 2,
        backgroundColor: "#00B8AB",
        justifyContent: "center",
        alignItems: "center",
        marginVertical: 10,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.20,
        shadowRadius: 1.41,
        elevation: 4,
    }
});