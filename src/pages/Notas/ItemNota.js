import React, { useState } from 'react';
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,

} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import styles from './styles';

const ItemNota = ({ item, setflagEdit, handleFavoritar, handleEditItem, handleRemoveNota, currentEditingId, setCurrentEditingId }) => {

    const [edit, setEdit] = useState(false);
    const [anotacao, setAnotacao] = useState(item.anotacao);

    const canPerformAction = () => {
        if (currentEditingId === null)
            return true;
        return currentEditingId === item.id;
    }

    const doEdit = () => {
        if (!canPerformAction()) {
            return;
        }
        setEdit(true);
        setCurrentEditingId(item.id);
        console.log('editrSS')
    }

    const doFav = () => {
        if (!canPerformAction()) {
            return;
        }
        handleFavoritar(item.id, item.favorito);
    }

    const doDelete = () => {
        if (!canPerformAction()) {
            return;
        }
        setEdit(false);
        setCurrentEditingId(null);
        handleRemoveNota(item.id);
    }

    const doUpdate = () => {
        if (!canPerformAction()) {
            return;
        }
        setEdit(false);
        setCurrentEditingId(null);
        handleEditItem({
            id: item.id,
            anotacao
        });
    }

    const getPencilColor = () => {
        if (currentEditingId === null)
            return "#343957";
        return "#EBEBEB";
    }

    const getTrashColor = () => {
        if (currentEditingId === null)
            return "#E90000";
        return "#EBEBEB";
    }

    return (
        <View
            style={{
                flexDirection: "row",
                justifyContent: "space-between",
                backgroundColor: "#FFFFFF",
                marginVertical: 5,
                marginHorizontal: 5,
                padding: 10,
                elevation: 6,
                shadowColor: '#000',
                shadowOffset: { width: 0, height: 4 },
                shadowOpacity: 0.5,
                shadowRadius: 5,
                borderRadius: 10,
            }}>
            <View style={styles.containerFavorito}>
                <TouchableWithoutFeedback
                    onPress={doFav}>
                    <Icon
                        name={'star'}
                        size={26}
                        color={item.favorito ? "#1C72BA" : "#ebebeb"}
                    />
                </TouchableWithoutFeedback>
            </View>

            {!edit &&
                <Text style={styles.containerTexto}>
                    {anotacao}
                </Text>
            }

            {edit && <TextInput
                style={styles.inputTextLista}
                autoCapitalize="none"
                autoCompleteType="off"
                autoCorrect={false}
                multiline
                underlineColorAndroid="transparent"
                value={anotacao}
                onChangeText={setAnotacao}
            />}

            <View style={styles.containerAcoes}>
                {!edit &&
                    <TouchableOpacity
                        style={styles.btnLista}
                        hitSlop={{ top: 5, bottom: 5, left: 0, right: 0 }}
                        onPress={doEdit}>
                        <Icon
                            name={'pencil'}
                            size={26}
                            color={getPencilColor()}
                        />
                    </TouchableOpacity>
                }
                {edit &&
                    <TouchableOpacity
                        style={styles.btnLista}
                        hitSlop={{ top: 5, bottom: 5, left: 0, right: 0 }}
                        onPress={doUpdate}>
                        <Icon
                            name={'check-bold'}
                            size={26}
                            color={"#343957"}
                        />
                    </TouchableOpacity>
                }

                <TouchableOpacity
                    style={styles.btnLista}
                    hitSlop={{ top: 5, bottom: 5, left: 0, right: 10 }}
                    onPress={doDelete}>
                    <Icon
                        name={'trash-can-outline'}
                        size={26}
                        color={getTrashColor()}
                    />
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default ItemNota;