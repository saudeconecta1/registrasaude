import React, { useState, useEffect, useCallback } from 'react';
import {
    View,
    Text,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Keyboard,
    BackHandler
} from 'react-native';

import { getUser } from '../../../src/utils';
import Database from '../../banco/Database';
import styles from './styles';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Header from '../../components/Header';
import { format } from 'date-fns';
import CovidAppContext from '../../contexts/CovidAppContext';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ItemNota from './ItemNota';

const Notas = () => {
    const db = new Database();
    const [notas, setNotas] = useState([]);
    const [anotacao, setAnotacao] = useState('');
    const [userLogado, setUserLogado] = useState('');
    const { setHasNotaContext } = React.useContext(CovidAppContext);
    const [loading, setLoading] = useState(false);
    const [currentEditingId, setCurrentEditingId] = useState(null);

    useEffect(() => {
        loadData();
    }, []);

    const loadData = useCallback(async () => {
        try {
            const dataUser = await getUser();
            const { user } = JSON.parse(dataUser);
            setUserLogado(user);
            const data = await db.listarNotas(user.id);
            if (data.length) {
                setHasNotaContext(true);
            } else {
                setHasNotaContext(false);
            }
            setNotas(data);
        } catch (err) {
            console.log(err);
        }
    });

    useEffect(() => () => console.log("unmount notas"), []);

    const handleEditItem = async (editedItem) => {
        console.log("EDITAR O ITEM: ", editedItem);
        setLoading(true)
        try {
            await db.updateNota(editedItem);
            loadData();
            Keyboard.dismiss();
        } catch (err) {
            console.log(err, 'error');
        } finally {
            setLoading(false);
        }
    }

    const handleFavoritar = async (id, statusFavorito) => {
        try {
            const fav = statusFavorito === 0 ? 1 : 0;
            await db.favoritarNota(id, fav);
            loadData();
        } catch (err) {
            console.log(err, 'error');
        }
    };

    async function handleNewNota() {
        if (anotacao) {
            try {
                const insertDate = new Date();
                const formattedDate = format(
                    insertDate,
                    "yyyy-MM-dd HH:mm:ss"
                );

                const newNota = {
                    idUser: userLogado.id,
                    anotacao: anotacao,
                    favorito: 0,
                    transactionDate: formattedDate,
                    status: 0
                };

                const nota = await db.adicionarNota(newNota);
                console.log('## Nota adicionada', nota.insertId);
                setAnotacao('');
                Keyboard.dismiss();
                loadData();
            } catch (error) {
                console.log(error);
            }
        }
    }

    function handleRemoveNota(id) {
        db.deleteNota(id);
        loadData();
    }

    return (
        <View style={styles.container}>
            <Header />

            <KeyboardAwareScrollView>
                <View style={[{ paddingBottom: 100 }]}>
                    {notas.map((item) =>
                        <ItemNota
                            item={item}
                            currentEditingId={currentEditingId}
                            setCurrentEditingId={setCurrentEditingId}
                            handleFavoritar={handleFavoritar}
                            handleEditItem={handleEditItem}
                            handleRemoveNota={handleRemoveNota}
                            key={item.id} />)}
                </View>
            </KeyboardAwareScrollView>

            {currentEditingId === null &&
                <View style={styles.containerInputNota}>
                    <TextInput
                        style={styles.inputTextNotas}
                        value={anotacao}
                        onChangeText={text => setAnotacao(text)}
                        underlineColorAndroid="transparent"
                        multiline
                        returnKeyType="next"
                        maxLength={250}
                    />
                    <TouchableOpacity
                        style={styles.btnNew}
                        onPress={() => handleNewNota()}>
                        <Icon name={'plus'} size={16} color={"#FFF"} />
                    </TouchableOpacity>
                </View>
            }
        </View>
    )
}

export default Notas;
