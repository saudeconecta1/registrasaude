import React, { useState, useEffect } from 'react';
import { Linking, View, ScrollView, Text, TouchableOpacity } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';
import styles from './styles';
import Header from '../../components/Header';
import Accordion from '../../components/Accordion';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const Config = () => {
    const navigation = useNavigation();

    useEffect(() => () => console.log("unmount config"), []);

    const handleSignout = async () => {
        try {
            await AsyncStorage.removeItem('@ListApp:userToken')
            navigation.navigate("Auth");
        } catch (e) {
            console.log('Failed to clear the async storage.')
        }
    }

    return (
        <View style={styles.container}>
            <Header />
            <ScrollView style={styles.scrollView}>
                <RenderAccordians />
                <TouchableOpacity
                    style={[styles.row, { backgroundColor: '#8AC868' }]}
                    onPress={handleSignout}>
                    <Text style={[styles.textoItemConfig]}>
                        Sair
                    </Text>
                </TouchableOpacity>
            </ScrollView>
        </View>
    )
};

const RenderAccordians = () => {

    function sobreApp() {
        return (
            <View style={styles.containerSobreApp}>
                <Text style={styles.textoSobreApp}>
                    Este aplicativo monitora onde estão os casos suspeitos de COVID-19.
                </Text>
                <Text style={styles.textoSobreApp}>
                    As informações coletadas geram relatórios importantes para a Atenção à Saúde na sua região.
                </Text>
                <Text style={styles.textoSobreApp}>
                    O aplicativo não está programado para sincronizar os dados automaticamente.
                </Text>
                <Text style={styles.textoSobreApp}>
                    Para ter informação mais precisa nos relatórios, é preciso que você faça a sincronização a cada
                    48 horas, no botão <Icon name={'sync'} size={24} color={'#545454'} />
                    na barra inferior do seu aplicativo.
                </Text>
                <Text style={styles.textoSobreApp}>
                    Seus registros serão mais um força para conter a COVID-19!
                </Text>
            </View>
        )
    }

    function protocoloApp() {
        return (
            <View>
                <Text style={styles.textoProtocolo}>
                    A Covid-19 é uma doença nova e traz uma série de questionamentos, inclusive, entre os profissionais de saúde.
                </Text>
                <Text style={styles.textoProtocolo}>
                    Mais do que nunca, seguir os protocolos corretos é fundamental para combater o novo coronavírus.
                </Text>
                <Text style={styles.textoProtocolo}>
                    Lave sempre as mãos, evite tocar no rosto e adote as medidas de etiqueta respiratória.
                </Text>
                <Text style={styles.textoProtocolo}>
                    Mantenha-se em dia com as recomendações do Ministério da Saúde pelo <Text
                        style={styles.TextStyle}
                        onPress={() => Linking.openURL('https://coronavirus.saude.gov.br')}>
                        https://coronavirus.saude.gov.br
                        </Text>
                </Text>
            </View>
        )
    }

    function creditosApp() {
        return (
            <View>
                <View style={styles.containerMembro}>
                    <Text style={styles.textoFuncao}>Desenvolvimento:</Text>
                    <Text style={styles.textoNome}>Leandro da Rocha Brandão</Text>
                </View>

                <View style={styles.containerMembro}>
                    <Text style={styles.textoFuncao}>UI/UX Designer:</Text>
                    <Text style={styles.textoNome}>Daniela Fontinele</Text>
                    <Text style={styles.textoNome}>Laura Gris Mota</Text>
                </View>

                <View style={styles.containerMembro}>
                    <Text style={styles.textoFuncao}>Responsáveis Técnicos:</Text>
                    <Text style={styles.textoNome}>Renata Bernardes David</Text>
                    <Text style={styles.textoProfissao}>Fisioterapeuta e Sanitarista</Text>
                </View>

                <Text style={styles.textoApp}>
                    Versão 1.0{"\n"}
                    2020 - Saúde Conecta
                </Text>
            </View>
        )
    }

    const [menu, setMenu] = useState([
        {
            id: 1,
            title: 'Protocolos',
            color: '#00B8AB',
            data: protocoloApp(),
        },
        {
            id: 2,
            title: 'Sobre o aplicativo',
            color: '#539D91',
            data: sobreApp()
        },
        {
            id: 3,
            title: 'Créditos',
            color: '#6FB37D',
            data: creditosApp()
        },
    ]);

    const items = [];
    {
        menu.map((item) => {
            items.push(
                <Accordion
                    key={item.id}
                    id={item.id}
                    title={item.title}
                    data={item.data}
                    color={item.color}
                />
            );
        })
    }

    return items;
};

export default Config;