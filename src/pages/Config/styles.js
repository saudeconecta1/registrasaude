import { StyleSheet } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFF",
    },
    item: {
        backgroundColor: "#CCC",
        borderColor: '#545454',
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        marginVertical: 15,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingVertical: 15,
        paddingLeft: 60,
        paddingRight: 20,
        marginRight: 45
    },
    row: {
        backgroundColor: "#CCC",
        borderColor: '#545454',
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        marginVertical: 15,
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        paddingVertical: 15,
        paddingLeft: 60,
        paddingRight: 20,
        marginRight: 45
    },
    textoItemConfig: {
        fontFamily: 'RobotoCondensed-Bold',
        fontSize: 18,
        color: "#FFF",
    },
    textoProtocolo: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: "#545454",
        lineHeight: 24,
        marginVertical: 10
    },
    containerMembro: {
        marginBottom: 30,
    },
    textoFuncao: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: "#545454",
        marginBottom: 5,
    },
    textoNome: {
        fontFamily: 'Roboto-Bold',
        fontSize: 16,
        color: "#545454",
        marginBottom: 5,
    },
    textoProfissao: {
        fontSize: 16,
        fontFamily: 'Roboto-Italic',
        color: "#545454",
        marginBottom: 5,
    },
    containerSobreApp: {
        paddingRight: 5,
    },
    textoSobreApp: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: "#545454",
        lineHeight: 24,
        marginVertical: 10,
    },
    TextStyle: {
        color: '#E91E63',
        textDecorationLine: 'underline'
    },
    textoApp: {
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        lineHeight: 24,
        color: "#545454",
    },
});