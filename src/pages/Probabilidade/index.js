import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';

import styles from './styles';
import Header from '../../components/Header';

export default function Probabilidade() {

    const [surtos, setSurtos] = useState([
        {
            id: 1,
            tipo: 'alta',
            titulo: 'ALTA',
            casosNotificados: 54,
            registrosLocais: 36,
            cor: '#F51222'
        },
        {
            id: 2,
            tipo: 'media',
            titulo: 'MÉDIA',
            casosNotificados: 23,
            registrosLocais: 12,
            cor: '#F5C812'
        },
        {
            id: 3,
            tipo: 'baixa',
            titulo: 'BAIXA',
            casosNotificados: 85,
            registrosLocais: 51,
            cor: '#00B8AB'
        }
    ]);

    const [surto, setSurto] = useState({});

    const randomSurto = () => {
        //seleciona surto aleatorio...
        let surto = surtos[Math.floor(Math.random() * surtos.length)];
        console.log('XXXX', surto);
        setSurto(surto);
    };

    useEffect(() => {
        randomSurto();
    }, []);

    return (
        <View style={styles.container}>
            <Header />

            <View style={styles.containerTexto}>
                <View style={styles.containerCenter}>
                    <Text style={styles.titulo}>
                        Probabilidade de surto
                    </Text>
                </View>
            </View>

            <View style={styles.containerCircle}>
                <View style={[styles.circle, { backgroundColor: surto.cor }]}>
                    <Text style={styles.texto}>{surto.titulo}</Text>
                </View>
            </View>

            <View style={styles.containerInfo}>
                <Text style={styles.textoNumero}>{surto.casosNotificados}</Text>
                <Text style={styles.textoInfo}>Casos Notificados</Text>
            </View>

            <View style={styles.containerInfo}>
                <Text style={styles.textoNumero}>{surto.registrosLocais}</Text>
                <Text style={styles.textoInfo}>Registros locais de sintomáticos*</Text>
            </View>

            <View style={styles.containerTextLink}>
                <Text style={styles.textoLink}>Veja possíveis ações para esse cenário</Text>
            </View>

            <View style={styles.containerLink}>
                <Text style={styles.link}>*Dados do: www.opendatasus.gov.br</Text>
            </View>
        </View>
    )
}