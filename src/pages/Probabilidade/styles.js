import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFF",
    },
    containerTexto: {
        paddingHorizontal: 20,
        paddingVertical: 30,
        borderRadius: 10,

    },
    containerCenter: {
        alignItems: 'center'
    },
    titulo: {
        fontFamily: 'RobotoCondensed-Regular',
        fontSize: 20,
        color: "#4E4E4E",
    },
    containerCircle: {
        alignItems: "center",
        marginBottom: 20,
    },
    circle: {
        width: 165,
        height: 165,
        borderRadius: 165 / 2,
        backgroundColor: "#F51222",
        justifyContent: "center",
        alignItems: "center",
        alignContent: "center",
        elevation: 6
    },
    texto: {
        fontFamily: 'RobotoCondensed-Regular',
        fontSize: 24,
        color: "#FFF",
    },
    containerInfo: {
        marginHorizontal: 40,
        marginVertical: 2,
        flexDirection: 'row',
        alignItems: 'baseline',

    },
    textoNumero: {
        fontFamily: 'Roboto-Regular',
        fontSize: 26,
        color: "#4E4E4E",
    },
    textoInfo: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: "#4E4E4E",
        marginLeft: 14,
    },
    containerTextLink: {
        alignItems: 'center',
        marginVertical: 20,
    },
    textoLink: {
        width: '40%',
        fontFamily: 'Roboto-Regular',
        textDecorationLine: "underline",
        fontSize: 14,
        color: "#4E4E4E",
        textAlign: 'center'
    },
    containerLink: {
        marginTop: 40,
        paddingHorizontal: 40,
    },
    link: {
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
        color: "#4E4E4E",
    },
});