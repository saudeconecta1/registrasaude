import { StyleSheet } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFF",
    },
    header: {
        flexDirection: "row",
        height: 190,
        marginBottom: 50,
        justifyContent: "space-around",
        alignItems: 'center',
    },
    scLogo: {
        marginTop: -60,
        marginLeft: -80,
    },
    logoImg: {
        width: 116,
        height: 50,
    },
    btnOption: {
        height: 48,
        marginBottom: 25,
        backgroundColor: "#00B8AB",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 50,
    },
    btnComecar: {
        height: 48,
        backgroundColor: "#8AC868",
        alignItems: "center",
        justifyContent: "center",
        borderColor: '#545454',
        borderRadius: 50,
    },
    textoOption: {
        fontFamily: 'Roboto-Medium',
        color: "#FFF",
        fontSize: 16
    },
    footer: {
        height: 190,
        marginTop: 70,
        alignItems: 'flex-end',
    },
    scLogoFooter: {
        marginTop: 0,
        marginRight: -30,
    },
});