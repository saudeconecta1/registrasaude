import React from 'react';
import { Image, View, Text, TouchableOpacity } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import scLogo from '../../assets/saudeconecta.png';
import logoImg from '../../assets/logo.png';

import styles from './styles';

function Home() {
    const navigation = useNavigation();

    const handleRegistro = () => {
        navigation.navigate('RegistroForm');
    };

    const handleEscala = () => {
        navigation.navigate('EscalaForm');
    };

    return (
        <View style={styles.container}>

            <View style={styles.header}>
                <Image source={scLogo} style={styles.scLogo} />
                <Image source={logoImg} style={styles.logoImg} />
            </View>

            <View style={{
                marginHorizontal: 40,
            }}>
                <TouchableOpacity
                    style={styles.btnOption}
                    onPress={handleRegistro}
                >
                    <Text style={styles.textoOption}>Condições de saúde</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={styles.btnComecar}
                    onPress={handleEscala}
                >
                    <Text style={styles.textoOption}>Escala de vulnerabilidade</Text>
                </TouchableOpacity>
            </View>

            <View style={styles.footer}>
                <Image source={scLogo} style={styles.scLogoFooter} />
            </View>

        </View>
    )
}

export default Home;
