import React from "react";
import { Alert, Text, SafeAreaView, View, StyleSheet } from "react-native";

import { useNavigation } from '@react-navigation/native';

import { AuthService } from '../../api';

import { Formik } from "formik";
import * as Yup from "yup";
import Header from '../../components/HeaderIntro';

import { Button } from "react-native-elements";
import FormInput from "../../components/FormInput";
import FormButton from "../../components/FormButton";
import ErrorMessage from "../../components/ErrorMessage";

const validationSchema = Yup.object().shape({
  email: Yup.string()
    .label("Email")
    .email("Entre com um email válido")
    .required("Digite um e-mail registrado")
});

function ForgotPassword() {
  const authService = new AuthService();
  const navigation = useNavigation();

  function goToLogin() {
    navigation.navigate("Login");
  }

  async function handlePasswordReset(values, actions) {
    const { email } = values;
    const formData = new FormData();
    formData.append('email', email);

    try {
      const result = await authService.esqueceuSenha(formData);
      console.log("Resultado!", result);
      console.log("E-mail de redefinição de senha enviado com sucesso");
      let msgSucesso = 'E-mail de redefinição de senha enviado com sucesso';
      Alert.alert('Aviso', msgSucesso,
        [{ text: 'OK' }],
        { cancelable: false },
      );

      navigation.navigate("Login");

    } catch (e) {
      const data = e.response.data;
      console.log('LOG>>>>>', data);
      const msg = Object.keys(data.errors).map(k => {
        return data.errors[k].join(', ')
      }).join(', ');
      Alert.alert('Aviso', msg,
        [{ text: 'OK' }],
        { cancelable: false },
      );
      actions.setSubmitting(false);
    }
  }

  return (
    <SafeAreaView style={styles.container}>
      <Header />
      <View style={styles.containerTitle}>
        <Text style={styles.title}>Esqueceu a senha?</Text>
      </View>
      <Formik
        initialValues={{ email: "" }}
        onSubmit={(values, actions) => {
          handlePasswordReset(values, actions);
        }}
        validationSchema={validationSchema}
      >
        {({
          handleChange,
          values,
          handleSubmit,
          errors,
          isValid,
          touched,
          handleBlur,
          isSubmitting
        }) => (
            <>
              <FormInput
                name="email"
                value={values.email}
                onChangeText={handleChange("email")}
                placeholder="E-mail"
                autoCapitalize="none"
                iconName="email-outline"
                iconColor="#545454"
                onBlur={handleBlur("email")}
              />
              <ErrorMessage errorValue={touched.email && errors.email} />
              <View style={styles.buttonContainer}>
                <FormButton
                  buttonType="outline"
                  onPress={handleSubmit}
                  title="Enviar"
                  buttonColor="#1C72BA"
                  disabled={!isValid || isSubmitting}
                  loading={isSubmitting}
                />
              </View>
              <ErrorMessage errorValue={errors.general} />
            </>
          )}
      </Formik>

      <Button
        title="Login"
        onPress={goToLogin}
        titleStyle={{
          fontFamily: 'Roboto-Regular',
          fontSize: 12,
          color: "#545454"
        }}
        type="clear"
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
  },
  containerTitle: {
    alignItems: "center",
    marginTop: 15,
    marginBottom: 20,
  },
  title: {
    fontFamily: 'RobotoCondensed-Bold',
    fontSize: 18,
    textTransform: "uppercase",
    color: "#4E4E4E"
  },
  buttonContainer: {
    margin: 25
  }
});

export default ForgotPassword;
