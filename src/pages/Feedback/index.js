import React, { useState, useEffect } from 'react';
import { View, Text } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import styles from './styles';
import Header from '../../components/Header';
import TipoCaso from '../../components/TipoCaso';
import FormButton from "../../components/FormButton";

export default function Feedback({ route }) {
    const navigation = useNavigation();

    const [tipoCaso, setTipoCaso] = useState(0);
    const [qtdCaso, setQtdCaso] = useState(0);

    useEffect(() => {
        setTipoCaso(route.params.caso);
        setQtdCaso(route.params.qtdCaso);
    }, [route.params.caso, route.params.qtdCaso]);

    useEffect(() => () => console.log("unmount feedback"), []);

    function navigateToRegistro() {
        navigation.reset({
            routes: [{ name: 'RegistroForm' }],
        });
    }

    return (
        <View style={styles.container}>
            <Header />

            <View style={styles.containerFeedback}>

                <View style={styles.containerTexto}>
                    <TipoCaso caso={tipoCaso} />
                </View>

                <View style={styles.containerCircle}>
                    <View style={styles.circle}>
                        <Text style={styles.textoAlerta}>Você já registrou</Text>
                        <Text style={styles.textoAlerta}>{qtdCaso}</Text>
                        <Text style={styles.textoAlerta}>
                            {`${qtdCaso > 1 ? 'casos suspeitos' : 'caso suspeito'}`}
                        </Text>
                        <Text style={styles.textoAlerta}>COVID-19.</Text>
                    </View>
                </View>

                <View style={styles.buttonContainer}>
                    <FormButton
                        buttonType="outline"
                        onPress={navigateToRegistro}
                        title="Efetuar novo registro"
                        buttonColor="#1C72BA"
                    />
                </View>
            </View>
        </View>
    )
}