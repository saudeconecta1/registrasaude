import { StyleSheet } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFF",
    },
    containerTexto: {
        alignItems: "center",
    },
    containerCircle: {
        alignItems: "center",
        marginVertical: 50,
    },
    circle: {
        width: 163,
        height: 163,
        borderRadius: 163 / 2,
        backgroundColor: "#00B8AB",
        justifyContent: "center",
        alignItems: "center",
        alignContent: "center",

    },
    textoAlerta: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: "#FFF",
    },
    label: {
        fontFamily: 'Roboto-Regular',
        color: "#FFF",
        fontSize: 14,
    },
    buttonContainer: {
        margin: 25
    }
});