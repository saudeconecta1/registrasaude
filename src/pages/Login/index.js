import React, { useState, useEffect, useRef } from 'react';
import AsyncStorage from '@react-native-community/async-storage';
import {
    ActivityIndicator,
    Alert,
    Image,
    Platform,
    View,
    SafeAreaView,
    Text,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Keyboard,
    KeyboardAvoidingView,
    BackHandler
} from 'react-native';

import api from '../../services/api';

import { Formik } from "formik";
import * as Yup from "yup";

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { TextInputMask } from 'react-native-masked-text';

import { useNavigation } from '@react-navigation/native';

import ErrorMessage from "../../components/ErrorMessage";

import styles from './styles';

import scLogo from '../../assets/saudeconecta.png';
import logoImg from '../../assets/logo.png';

const validationSchema = Yup.object().shape({
    cpf: Yup.string()
        .label("CPF")
        .required("Digite seu CPF"),
    password: Yup.string()
        .label("Password")
        .required("Digite uma senha")
});

function Login() {
    const navigation = useNavigation();
    const [loading, setLoading] = useState(false);
    const inputRef = useRef(null);

    useEffect(() => {
        BackHandler.addEventListener('hardwareBackPress', handlerBackPress);
        return () => {
            BackHandler.removeEventListener('hardwareBackPress', handlerBackPress);
        };
    }, [handlerBackPress]);

    const handlerBackPress = () => {
        BackHandler.exitApp();
        return true;
    }

    function navigateToForgotPassword() {
        navigation.navigate('ForgotPassword');
    }

    function navigateToSignup() {
        navigation.navigate('Signup');
    }

    async function saveUser(user) {
        await AsyncStorage.setItem('@ListApp:userToken', JSON.stringify(user))
    }

    function buildForm(values) {
        values.cpf = values.cpf.replace(/[^\d]+/g, '');
        values.device_name = 'mobile';

        return values;
    }

    async function handleOnLogin(values, actions) {
        const form = buildForm(values);
        setLoading(true);

        try {
            const response = await api.post('/sanctum/token', form);
            const user = response.data;
            console.log(user);
            await saveUser(user);

            setLoading(false);
            actions.setSubmitting(false);

            navigation.reset({
                index: 0,
                routes: [{ name: 'App' }],
            });

            Keyboard.dismiss();
        } catch (err) {
            setLoading(false);
            actions.setSubmitting(false);

            Alert.alert('Aviso', 'CPF ou Senha inválidos',
                [{ text: 'OK' }],
                { cancelable: false },
            );
        }
    }

    return (
        <KeyboardAvoidingView
            behavior={Platform.OS === "ios" ? "padding" : null}
            style={{ flex: 1, }}
        >
            <SafeAreaView style={styles.container}>
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                    <View style={styles.inner}>

                        <View style={styles.header}>
                            <Image source={scLogo} style={styles.scLogo} />
                            <Image source={logoImg} style={styles.logoImg} />
                        </View>

                        <Formik
                            initialValues={{ cpf: "", password: "" }}
                            onSubmit={(values, actions) => {
                                handleOnLogin(values, actions);
                            }}
                            validationSchema={validationSchema}
                        >
                            {({
                                handleChange,
                                values,
                                handleSubmit,
                                errors,
                                isValid,
                                touched,
                                handleBlur,
                                isSubmitting
                            }) => (
                                    <View style={styles.form}>
                                        <View style={styles.boxTitles}>
                                            <Text style={styles.title}>REGISTRA SAÚDE</Text>
                                        </View>

                                        <View style={styles.containerInputs}>
                                            <View style={styles.containerInput}>
                                                <Icon
                                                    name={'account-outline'}
                                                    size={22}
                                                    color="#545454" />
                                                <TextInputMask
                                                    style={styles.input}
                                                    type={'cpf'}
                                                    value={values.cpf}
                                                    onChangeText={handleChange("cpf")}
                                                    onBlur={handleBlur("cpf")}
                                                    placeholder="CPF"
                                                    placeholderTextColor="#545454"
                                                    underlineColorAndroid="transparent"
                                                    returnKeyType="next"
                                                    onSubmitEditing={() => { inputRef.current.focus(); }}
                                                    blurOnSubmit={false}
                                                />
                                            </View>
                                            <ErrorMessage errorValue={touched.cpf && errors.cpf} />

                                            <View style={styles.containerInput}>
                                                <Icon
                                                    style={styles.iconStyle}
                                                    name={'lock-outline'}
                                                    size={22}
                                                    color="#545454" />
                                                <TextInput
                                                    style={styles.input}
                                                    ref={inputRef}
                                                    value={values.password}
                                                    onChangeText={handleChange("password")}
                                                    placeholder="Senha"
                                                    placeholderTextColor="#545454"
                                                    secureTextEntry
                                                    autoCapitalize="none"
                                                    autoCorrect={false}
                                                    underlineColorAndroid="transparent"
                                                    returnKeyType="send"
                                                />
                                            </View>
                                            <ErrorMessage errorValue={touched.password && errors.password} />

                                            <View style={styles.containerAuth}>
                                                <TouchableOpacity
                                                    style={styles.forgotPassword}
                                                    onPress={navigateToForgotPassword}>
                                                    <Text style={styles.esqueceu}>Esqueceu a senha?</Text>
                                                </TouchableOpacity>
                                            </View>
                                        </View>


                                        <View style={styles.containerNovoCadastro}>
                                            <TouchableOpacity
                                                style={styles.forgotPassword}
                                                onPress={navigateToSignup}>
                                                <Text style={styles.novoCadastro}>Novo cadastro</Text>
                                            </TouchableOpacity>
                                        </View>

                                        <View style={styles.containerBtn}>
                                            <TouchableOpacity
                                                style={styles.btnEntrar}
                                                onPress={handleSubmit}
                                                disabled={!isValid || isSubmitting}
                                            >
                                                {loading ? (
                                                    <ActivityIndicator size="small" color="#FFF" />
                                                ) : (
                                                        <Text style={styles.entrar}>ENTRAR</Text>
                                                    )}
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                )}
                        </Formik>
                    </View>
                </TouchableWithoutFeedback>
            </SafeAreaView>
        </KeyboardAvoidingView >
    )
}

export default Login;
