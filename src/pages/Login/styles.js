import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "flex-end",
        backgroundColor: "#FFF",
    },
    inner: {
        justifyContent: "flex-end",
    },
    header: {
        height: 160,
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: 'center',
    },
    scLogo: {
        marginTop: -210,
        marginLeft: -70,
    },
    logoImg: {
        marginTop: -150,
        width: 116,
        height: 50,
    },
    boxTitles: {
        alignItems: "center",
        marginTop: 40,
        marginBottom: 20,
    },
    title: {
        fontFamily: 'RobotoCondensed-Bold',
        fontSize: 20,
        color: "#545454",
    },
    containerInputs: {
        marginHorizontal: 35,
        marginTop: 10,
        marginBottom: 50,
    },
    containerAuth: {
        alignItems: "flex-end",
        paddingHorizontal: 25
    },
    forgotPassword: {

    },
    esqueceu: {
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
        color: "#545454"
    },
    containerNovoCadastro: {
        alignItems: "center"
    },
    novoCadastro: {
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
        color: "#545454"
    },
    containerInput: {
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: '#545454',
        borderRadius: 50,
        alignItems: 'center',
        marginHorizontal: 20,
        marginVertical: 2,
        paddingLeft: 20,
        paddingRight: 10,
        alignItems: 'center',
        flexDirection: 'row',
    },
    input: {
        width: '90%',
        marginLeft: 15,
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        color: "#545454",
    },
    containerBtn: {
        marginTop: "10%",
    },
    btnEntrar: {
        height: 48,
        backgroundColor: "#1C72BA",
        alignItems: "center",
        justifyContent: "center"
    },
    entrar: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: "#FFF"
    },
});