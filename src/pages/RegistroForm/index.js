import React, { useState, useEffect, useCallback } from 'react';
import {
    ActivityIndicator,
    Image,
    Keyboard,
    //PermissionsAndroid,
    ScrollView,
    Text,
    TextInput,
    TouchableOpacity,
    View,
    YellowBox
} from 'react-native';

YellowBox.ignoreWarnings(['VirtualizedLists should never be nested']);

import { TextInputMask } from 'react-native-masked-text';

import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

//import Geolocation from 'react-native-geolocation-service';

import { getUser } from '../../../src/utils';

import { useNavigation } from '@react-navigation/native';

import { format } from 'date-fns';

import IconTermometro from '../../assets/termometro.png';
import IconFala from '../../assets/fala.png';
import IconPulmao from '../../assets/pulmao.png';
import IconContato from '../../assets/contato.png';
import IconDorGarganta from '../../assets/garganta.png';
import styles from './styles';
import Header from '../../components/Header';

import Database from '../../banco/Database';

import CovidAppContext from '../../contexts/CovidAppContext';

const APIKEY = 'AIzaSyD1nZwGtCHk-GTvXW-iu63BRSlqKCX_wFo';

function RegistroForm() {
    const db = new Database();

    const navigation = useNavigation();
    const { hasNotaContext, setHasNotaContext } = React.useContext(CovidAppContext);
    const { hasSyncContext, setHasSyncContext } = React.useContext(CovidAppContext);

    const ref = React.useRef();

    React.useEffect(() => {
        ref.current.setAddressText('');
    }, []);

    const [cpf, setCpf] = useState('');
    const [teveFebre, setTeveFebre] = useState(null);
    const [teveTosse, setTeveTosse] = useState(null);
    const [teveDificuldadeRespirar, setTeveDificuldadeRespirar] = useState(null);
    const [teveContato, setTeveContato] = useState(null);
    const [teveDorGarganta, setTeveDorGarganta] = useState(null);

    const [txtAnotacao, setTxtAnotacao] = useState('');
    const [userLogado, setUserLogado] = useState(null);
    const [disabled, setDisabled] = useState(true);
    const [hasLocationPermission, setHasLocationPermission] = useState(false);
    //const [userPosition, setUserPosition] = useState(false);
    const [logradouro, setLogradouro] = useState('');
    const [geometria, setGeometria] = useState({});
    const [loading, setLoading] = useState(false);

    const [validateCpf, setValidateCpf] = useState(false);
    const [validateLogradouro, setValidateLogradouro] = useState(false);

    useEffect(() => {
        getUser().then((data) => {
            const { user } = JSON.parse(data);
            setUserLogado(user);
        })
    }, []);

    useEffect(() => {
        console.log('# VERIFICAR FORM...');
        verificaForm();
    }, [
        logradouro,
        cpf,
        teveFebre,
        teveTosse,
        teveDificuldadeRespirar,
        teveContato,
        teveDorGarganta,
    ]);

    useEffect(() => () => console.log("unmount registro_form"), []);

    /*
    async function verifyLocationPermission() {
        try {
            const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log('permissão concedida');
                setHasLocationPermission(true);
            } else {
                console.error('permissão negada');
                setHasLocationPermission(false);
            }
        } catch (err) {
            console.warn(err);
        }
    }

    useEffect(() => {
        verifyLocationPermission();

        if (hasLocationPermission) {
            Geolocation.getCurrentPosition(
                (position) => {
                    console.log(position);
                    setUserPosition({
                        latitude: position.coords.latitude,
                        longitude: position.coords.longitude,
                    });
                },
                (error) => {
                    // See error code charts below.
                    console.log(error.code, error.message);
                },
                { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
            );
        }
    }, [hasLocationPermission]);
    */

    function verificaForm() {
        console.log('LOGRADOURO: ', logradouro);
        console.log('CPF: ', cpf.length);

        if (logradouro !== undefined) {
            console.log('Logradouro ok');
            setValidateLogradouro(false);
        } else {
            console.log('Logradouro vazio');
            setValidateLogradouro(true);
        }

        if (cpf.length === 14) {
            setValidateCpf(false);
            Keyboard.dismiss();
        } else {
            setValidateCpf(true);
        }

        if (logradouro !== undefined
            && cpf !== ''
            && cpf.length === 14
            && teveFebre !== null
            && teveTosse !== null
            && teveDificuldadeRespirar !== null
            && teveContato !== null
            && teveDorGarganta !== null) {
            setDisabled(false);
        } else {
            setDisabled(true);
        }
    }

    async function navigateToFeedback() {
        setLoading(true);
        let tipoCaso = 0;

        const cpfVisita = cpf.replace(/[^\d]+/g, '');

        if (
            (teveFebre && teveTosse)
            || (teveFebre && teveDificuldadeRespirar)
            || (teveFebre && teveDorGarganta)
            || (teveFebre && teveTosse && teveDorGarganta && teveDificuldadeRespirar)
        ) {
            //Caso suspeito!
            tipoCaso = 1;
        } else if (
            (teveFebre && (teveTosse || teveDorGarganta || teveDificuldadeRespirar || teveContato))
            || (teveTosse && (teveFebre || teveDorGarganta || teveDificuldadeRespirar || teveContato))
            || (teveDorGarganta && (teveTosse || teveFebre || teveDificuldadeRespirar || teveContato))
            || (teveDificuldadeRespirar && (teveTosse || teveDorGarganta || teveFebre || teveContato))
            || (teveContato && (teveTosse || teveDorGarganta || teveDificuldadeRespirar || teveFebre))
            || (teveTosse && teveDificuldadeRespirar)
            || (teveTosse && teveContato)
            || (teveTosse && teveDorGarganta)
            || (teveDificuldadeRespirar && teveContato)
            || (teveDificuldadeRespirar && teveDorGarganta)
            || (teveContato && teveDorGarganta)
        ) {
            //Acompanhe de perto!
            tipoCaso = 2;
        } else if (
            !teveFebre
            && !teveTosse
            && !teveDorGarganta
            && !teveDificuldadeRespirar
            && !teveContato
        ) {
            //Obrigado por colaborar!
            tipoCaso = 3;
        } else {
            tipoCaso = 3;
        }

        const relatorioJson = {
            idUser: userLogado.id,
            json: JSON.stringify({
                cpfVisita,
                teveFebre,
                teveTosse,
                teveDificuldadeRespirar,
                teveContato,
                teveDorGarganta,
                anotacao: txtAnotacao,
                logradouro: logradouro,
                latitude: geometria.lat,
                longitude: geometria.lng
            }),
            tipoCaso: tipoCaso,
            status: 0
        };

        console.log('::Relatório JSON: ', relatorioJson);

        try {
            setLoading(true);

            const insert = await db.adicionarRelatorio(relatorioJson);

            if (txtAnotacao) {
                const insertDate = new Date();
                const formattedDate = format(
                    insertDate,
                    "yyyy-MM-dd HH:mm:ss"
                );

                const nota = await db.adicionarNota({
                    idUser: userLogado.id,
                    anotacao: txtAnotacao,
                    favorito: 0,
                    transactionDate: formattedDate,
                    status: 0
                });
                setHasNotaContext(true);
                console.log('::NOTA SALVA COM SUCESSO!', nota);
            }

            const select = await db.getQuantidadeCasosSuspeitos(userLogado.id);

            console.log('::RELATORIO SALVO COM SUCESSO!', insert);
            console.log('::CONSULTA SQL!', select.qtd);

            clearForm();
            setLoading(false);
            setHasSyncContext(true);

            navigation.navigate('Feedback', {
                caso: tipoCaso,
                qtdCaso: select.qtd
            });
        } catch (e) {
            setDisabled(false);
            setLoading(false);
            console.log('::Erro ao inserir informação', e);
        }
    }

    function isFalse(value) {
        return value !== null && value === false;
    }

    function clearForm() {
        setLogradouro('');
        setCpf('');
        setTeveFebre(null);
        setTeveTosse(null);
        setTeveDificuldadeRespirar(null);
        setTeveContato(null);
        setTeveDorGarganta(null);
        setTxtAnotacao('');
        setGeometria({});
        ref.current.setAddressText('');
        //setUserPosition(false);
    }

    return (
        <View style={styles.container}>
            <Header />

            <ScrollView style={styles.scrollView} keyboardShouldPersistTaps='always'>
                <View style={{
                    marginBottom: 8,
                    marginHorizontal: 30,
                }} keyboardShouldPersistTaps="handled">
                    <Text style={styles.textoLabel}>Logradouro</Text>
                    <GooglePlacesAutocomplete
                        placeholder=""
                        placeholderTextColor="#333"
                        minLength={2}
                        autoFocus={false}
                        returnKeyType={'default'}
                        fetchDetails={true}
                        listViewDisplayed={false}
                        onPress={(data, details = null) => {
                            console.log(JSON.stringify(data));
                            setLogradouro(details.formatted_address);
                            setGeometria(details.geometry.location);
                        }}
                        onFail={
                            error => {
                                console.error('Error Google Places API', error);
                                setLogradouro('');
                                setGeometria({});
                            }
                        }
                        query={{
                            key: APIKEY,
                            language: 'pt-BR',
                            components: 'country:br',
                        }}
                        ref={ref}
                        textInputProps={{
                            autoCapitalize: 'none',
                            autoCorrect: false,
                            selectionColor: '#000',
                            onBlur: () => Keyboard.dismiss,
                            onChangeText: (value) => {
                                if (value.length === 0) {
                                    setLogradouro();
                                }
                            },
                        }}
                        enablePoweredByContainer={false}
                        styles={{
                            textInputContainer: {
                                backgroundColor: 'rgba(0,0,0,0)',
                                borderTopWidth: 0,
                                borderBottomWidth: 0,
                                marginHorizontal: -8,
                            },
                            textInput: {
                                height: 50,
                                color: '#5d5d5d',
                                fontFamily: 'Roboto-Regular',
                                fontSize: 18,
                                borderWidth: 1,
                                borderColor: validateLogradouro ? "#f90000" : "#545454",
                                borderWidth: validateLogradouro ? 1.5 : 1,
                                borderRadius: 25,
                                paddingLeft: 20,
                            },
                            predefinedPlacesDescription: {
                                color: '#1faadb',
                            }
                        }}
                    />
                </View>

                <View style={styles.containerCpf}>
                    <Text style={styles.textoLabel}>CPF</Text>
                    <TextInputMask
                        style={[styles.inputCpf,
                        validateCpf ? { borderColor: "#f90000", borderWidth: 1.5 }
                            : { borderColor: "#545454", borderWidth: 1 }
                        ]}
                        type={'cpf'}
                        value={cpf}
                        onChangeText={text => setCpf(text)}
                        underlineColorAndroid="transparent"
                    />
                </View>

                <View style={[styles.containerPerguntas, { backgroundColor: "#8AC868" }]}>
                    <View style={styles.containerImage}>
                        <Image source={IconTermometro} style={styles.image} />
                    </View>
                    <View style={styles.containerPergunta}>
                        <Text style={styles.textoPergunta}>
                            Teve febre os últimos 14 dias?
                        </Text>

                        <View style={styles.containerOpcoes}>
                            <TouchableOpacity
                                onPress={() => setTeveFebre(true)}
                                style={[styles.btnQuestao, teveFebre
                                    ? styles.btnSelected
                                    : null]}>
                                <Text style={styles.label}>Sim</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => setTeveFebre(false)}
                                style={[styles.btnQuestao, isFalse(teveFebre)
                                    ? styles.btnSelected : null]}>
                                <Text style={styles.label}>Não</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

                <View style={[styles.containerPerguntas, { backgroundColor: "#6FB37D" }]}>
                    <View style={styles.containerImage}>
                        <Image source={IconFala} style={styles.image} />
                    </View>
                    <View style={styles.containerPergunta}>
                        <Text style={styles.textoPergunta}>Teve tosse nesse período?</Text>

                        <View style={styles.containerOpcoes}>
                            <TouchableOpacity
                                onPress={() => setTeveTosse(true)}
                                style={[styles.btnQuestao, teveTosse
                                    ? styles.btnSelected
                                    : null]}>
                                <Text style={styles.label}>Sim</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => setTeveTosse(false)}
                                style={[styles.btnQuestao, isFalse(teveTosse)
                                    ? styles.btnSelected
                                    : null]}>
                                <Text style={styles.label}>Não</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

                <View style={[styles.containerPerguntas, { backgroundColor: "#539D91" }]}>
                    <View style={styles.containerImage}>
                        <Image source={IconPulmao} style={styles.image} />
                    </View>

                    <View style={styles.containerPergunta}>
                        <Text style={styles.textoPergunta}>
                            Teve dificuldade para respirar nos últimos 14 dias?
                            </Text>

                        <View style={styles.containerOpcoes}>
                            <TouchableOpacity
                                onPress={() => setTeveDificuldadeRespirar(true)}
                                style={[styles.btnQuestao, teveDificuldadeRespirar
                                    ? styles.btnSelected
                                    : null]}>
                                <Text style={styles.label}>Sim</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => setTeveDificuldadeRespirar(false)}
                                style={[styles.btnQuestao, isFalse(teveDificuldadeRespirar)
                                    ? styles.btnSelected
                                    : null]}>
                                <Text style={styles.label}>Não</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

                <View style={[styles.containerPerguntas, { backgroundColor: "#1C72BA" }]}>
                    <View style={styles.containerImage}>
                        <Image source={IconDorGarganta} style={styles.image} />
                    </View>
                    <View style={styles.containerPergunta}>
                        <Text style={styles.textoPergunta}>
                            Teve dor de garganta nesse período?
                        </Text>

                        <View style={styles.containerOpcoes}>
                            <TouchableOpacity
                                onPress={() => setTeveDorGarganta(true)}
                                style={[styles.btnQuestao, teveDorGarganta
                                    ? styles.btnSelected
                                    : null]}>
                                <Text style={styles.label}>Sim</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => setTeveDorGarganta(false)}
                                style={[styles.btnQuestao, isFalse(teveDorGarganta)
                                    ? styles.btnSelected
                                    : null]}>
                                <Text style={styles.label}>Não</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

                <View style={[styles.containerPerguntas, { backgroundColor: "#3888A6" }]}>
                    <View style={styles.containerImage}>
                        <Image source={IconContato} style={styles.image} />
                    </View>
                    <View style={styles.containerPergunta}>
                        <Text style={styles.textoPergunta}>
                            Teve contato com alguém que teve esses mesmos sintomas?
                            </Text>

                        <View style={styles.containerOpcoes}>
                            <TouchableOpacity
                                onPress={() => setTeveContato(true)}
                                style={[styles.btnQuestao, teveContato
                                    ? styles.btnSelected
                                    : null]}>
                                <Text style={styles.label}>Sim</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                onPress={() => setTeveContato(false)}
                                style={[styles.btnQuestao, isFalse(teveContato)
                                    ? styles.btnSelected
                                    : null]}>
                                <Text style={styles.label}>Não</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>

                <View style={styles.containerNotas}>
                    <Text style={styles.textoNotas}>Notas</Text>

                    <View style={styles.containerInputNota}>
                        <TextInput
                            style={styles.inputTextNotas}
                            autoCapitalize="none"
                            autoCompleteType="off"
                            underlineColorAndroid="transparent"
                            enablesReturnKeyAutomatically={true}
                            multiline
                            numberOfLines={3}
                            value={txtAnotacao}
                            onChangeText={text => setTxtAnotacao(text)}
                        />
                    </View>
                </View>

                <View style={styles.containerFinalizar}>
                    {disabled ? (
                        <>
                            <TouchableOpacity
                                style={[styles.btnFinalizar, { backgroundColor: "#CECECE" }]}
                            >
                                <Text style={styles.label}>Aguardando preenchimento</Text>
                            </TouchableOpacity>
                            <View style={{ alignItems: "center", justifyContent: "center" }}>
                                <Text>Algum item está sem resposta.</Text>
                            </View>
                        </>
                    ) : (
                            <TouchableOpacity
                                style={[styles.btnFinalizar, { backgroundColor: "#1C72BA" }]}
                                onPress={navigateToFeedback}
                            >
                                {loading ? (
                                    <ActivityIndicator size="small" color="#FFF" />
                                ) : (
                                        <Text style={styles.label}>Finalizar registro</Text>
                                    )}
                            </TouchableOpacity>
                        )}
                </View>
            </ScrollView>
        </View>
    )
}

export default RegistroForm;
