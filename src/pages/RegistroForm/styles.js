import { StyleSheet } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFF",
    },
    containerCpf: {
        marginTop: 20,
        marginBottom: 20,
        marginHorizontal: 30,
    },
    inputCpf: {
        borderRadius: 50,
        alignItems: "center",
        paddingHorizontal: 20,
        fontFamily: 'Roboto-Regular',
        fontSize: 18,
        color: "#545454",
    },
    textoLabel: {
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        color: "#545454",
    },
    input: {
        width: '90%',
        marginLeft: 10,
        fontFamily: 'Roboto-Medium',
        fontSize: 18,
        color: "#545454",
    },
    containerPerguntas: {
        borderColor: '#545454',
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        marginVertical: 10,
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        paddingVertical: 15,
        marginRight: 30
    },
    containerPerguntasViagem: {
        backgroundColor: "#1C72BA",
        borderColor: '#545454',
        borderTopRightRadius: 10,
        borderBottomRightRadius: 10,
        marginVertical: 10,
        paddingVertical: 15,
        marginRight: 30,
    },
    containerDestino: {
        marginTop: 10,
        marginLeft: 100
    },
    containerPergunta: {
        width: 260,
    },
    textoPergunta: {
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        color: "#FFF",
    },
    containerOpcoes: {
        flexDirection: "row",
        marginVertical: 10,
    },
    btnQuestao: {
        width: 72,
        borderWidth: 1,
        borderColor: "#FFF",
        borderRadius: 20,
        height: 40,
        alignItems: "center",
        justifyContent: "center",
        marginRight: 30,
    },
    btnSelected: {
        borderWidth: 0,
        backgroundColor: 'rgba(52, 52, 52, 0.3)',
        elevation: 2
    },
    label: {
        fontFamily: 'Roboto-Medium',
        fontSize: 14,
        color: "#FFF",
    },
    textInput: {
        width: 250,
        height: 40,
        paddingLeft: 6,
        borderColor: 0,
        borderBottomColor: "#FFF",
        borderWidth: 1,
        color: "#FFF"
    },
    containerViajou: {
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
    },
    containerNotas: {
        paddingHorizontal: 30,
        marginVertical: 30
    },
    textoNotas: {
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        color: "#4E4E4E",
        marginBottom: 10,
    },
    inputTextNotas: {
        alignItems: "flex-start",
        textAlignVertical: "top",
        paddingHorizontal: 10,
        borderWidth: 2,
        borderColor: '#DBDBDB',
        borderRadius: 10,
        backgroundColor: "#FFFFFF",
    },
    containerFinalizar: {
        flex: 1,
        justifyContent: "center",
        margin: 25,
    },
    btnFinalizar: {
        paddingVertical: 10,
        paddingHorizontal: 30,
        marginHorizontal: 40,
        borderRadius: 25,
        justifyContent: "center",
        alignItems: "center",
    }
});