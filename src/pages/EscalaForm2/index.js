import React, { useState, useCallback } from 'react';
import { View, Text, TouchableOpacity, ScrollView, } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import { RadioButton } from 'react-native-paper';

import InputSpinner from 'react-native-input-spinner';

import styles from './styles';

import Header from '../../components/HeaderEscala';

function EscalaForm2({ route }) {
    const navigation = useNavigation();

    let totalGeral = route.params.total ? route.params.total : 0;
    const [qtdComodos, setQtdComodos] = useState(0);
    const [qtdMoradores, setQtdMoradores] = useState(0);

    const [erroComodos, setErroComodos] = useState(false);
    const [erroMoradores, setErroMoradores] = useState(false);

    const lista = [
        {
            id: 12,
            title: 'Qual o destino do lixo?',
            status: false,
            error: false,
            data: [
                {
                    label: 'Coletado',
                    value: 1,
                    score: 0,
                },
                {
                    label: 'Queimado/enterrado',
                    value: 2,
                    score: 3,
                },
                {
                    label: 'Céu aberto',
                    value: 3,
                    score: 3,
                },
            ]
        },
        {
            id: 13,
            title: 'Qual o destino das fezes e da urina?',
            status: false,
            error: false,
            data: [
                {
                    label: 'Fossa',
                    value: 1,
                    score: 3,
                },
                {
                    label: 'Rede de esgoto',
                    value: 2,
                    score: 0,
                },
                {
                    label: 'Céu aberto',
                    value: 3,
                    score: 3,
                },
            ]
        },
        {
            id: 14,
            title: 'Como é o tratamento da água? ',
            status: false,
            error: false,
            data: [
                {
                    label: 'Filtração',
                    value: 1,
                    score: 3,
                },
                {
                    label: 'Fervura',
                    value: 2,
                    score: 3,
                },
                {
                    label: 'Cloração',
                    value: 3,
                    score: 0,
                },
                {
                    label: 'Sem tratamento',
                    value: 4,
                    score: 3,
                },
            ]
        },
    ];

    const [questoes, setQuestoes] = useState(lista);

    const checkQuestao = (questao, object) => {
        let i;
        let opcoes = object.data;

        for (i = 0; i < opcoes.length; i++) {
            if (opcoes[i].isChecked === 'checked') {
                opcoes[i].isChecked = 'unchecked';
            }
        }

        questao.isChecked = "checked";
        object.status = true;
        object.error = false;

        const index = questoes.findIndex(obj => obj.id === object.id);
        const newTodos = [...questoes];
        newTodos[index] = object;

        setQuestoes(newTodos);
    };

    const verificarQuestoes = () => {
        let caso = 0;
        let fatorRisco = 0;
        let total = 0;
        let baixasCondicoesSaneamento = 0;
        let relacaoMoradorComodo = 0;
        let count = 0;

        questoes.map((questao) => {
            if (questao.status) {
                console.log(`A questão ${questao.id} respondida!`);

                questao.data.map((item) => {
                    if (item.isChecked === 'checked') {
                        total += item.score;
                    } else {
                        total += 0;
                    }
                });
            } else {
                console.log(`A questão ${questao.id} não foi respondida!`);
                questao.error = true;

                const index = questoes.findIndex(obj => obj.id === questao.id);
                const newTodos = [...questoes];
                newTodos[index] = questao;
                setQuestoes(newTodos);

                count = 1;
            }
        });

        setErroComodos(qtdComodos === 0 ? true : false);
        setErroMoradores(qtdMoradores === 0 ? true : false);

        if (qtdMoradores > qtdComodos) {
            relacaoMoradorComodo = 3;
        } else if (qtdMoradores === qtdComodos) {
            relacaoMoradorComodo = 2;
        }

        baixasCondicoesSaneamento = total > 0 ? 3 : 0;

        fatorRisco = totalGeral + relacaoMoradorComodo + baixasCondicoesSaneamento;

        if (count === 0 && qtdComodos > 0 && qtdMoradores > 0) {

            if (fatorRisco <= 4) {
                caso = 1;
                //console.log('R0 - risco habitual');
            } else if (fatorRisco === 5 || fatorRisco === 6) {
                caso = 2;
                //console.log('R1 - risco menor');
            } else if (fatorRisco === 7 || fatorRisco === 8) {
                caso = 3;
                //console.log('R2 - risco médio');
            } else {
                caso = 4;
                //console.log('R3 - risco máximo');
            }

            navigation.navigate('FeedbackEscala', {
                caso: caso,
                pontos: fatorRisco
            });
        }
    };

    return (
        <View style={styles.container}>
            <Header />
            <ScrollView
                style={styles.scrollView}
                keyboardShouldPersistTaps='always'
            >
                <View style={{ flex: 1, backgroundColor: "#FFF" }}>
                    {questoes.map((object, d) =>
                        <View
                            key={d}
                            style={[
                                styles.containerQuestao,
                                object.error ?
                                    { borderColor: '#FF0000', borderWidth: 1.5 } :
                                    { borderColor: '#707070', }]}>

                            <View style={styles.containerTituloQuestao}>
                                <Text style={styles.texto1}>{object.title}</Text>
                            </View>

                            {object.data.map((questao, i) =>
                                <View key={i}>
                                    <View style={styles.questaoCard}>
                                        <RadioButton
                                            value={questao.value}
                                            status={questao.isChecked}
                                            color={'#8AC868'}
                                            onPress={() => checkQuestao(questao, object)}
                                        />
                                        <Text style={styles.texto2}>{questao.label}</Text>
                                    </View>
                                </View>
                            )}
                        </View>
                    )}

                    <View style={[styles.containerQuestao, erroComodos ?
                        { borderColor: '#FF0000', borderWidth: 1.5 } :
                        { borderColor: '#707070', }]}
                    >
                        <View style={styles.questaoInput}>
                            <Text style={styles.texto2}>
                                Quantidade de cômodos no domicílio:
                            </Text>

                            <InputSpinner
                                max={99}
                                min={0}
                                step={1}
                                color={"#8AC868"}
                                colorMax={"#1C72BA"}
                                colorMin={"#00B8AB"}
                                value={qtdComodos}
                                onChange={(num) => {
                                    setQtdComodos(num);
                                    setErroComodos(num === 0 ? true : false);
                                }}
                                rounded={false}
                                height={40}
                                style={{
                                    alignItems: 'center',
                                    marginTop: 20,
                                }}
                            />
                        </View>
                    </View>

                    <View style={[styles.containerQuestao, erroMoradores ?
                        { borderColor: '#FF0000', borderWidth: 1.5 } :
                        { borderColor: '#707070', }]}
                    >
                        <View style={styles.questaoInput}>
                            <Text style={styles.texto2}>
                                Quantidade de moradores no domicílio:
                            </Text>
                            <InputSpinner
                                max={99}
                                min={0}
                                step={1}
                                color={"#8AC868"}
                                colorMax={"#1C72BA"}
                                colorMin={"#00B8AB"}
                                value={qtdMoradores}
                                onChange={(num) => {
                                    setQtdMoradores(num);
                                    setErroMoradores(num === 0 ? true : false);
                                }}
                                rounded={false}
                                height={40}
                                style={{
                                    alignItems: 'center',
                                    marginTop: 20,
                                }}
                            />
                        </View>
                    </View>
                </View>
                <View style={styles.containerBtn}>
                    <TouchableOpacity
                        style={styles.btnAction}
                        onPress={() => verificarQuestoes()}>
                        <Text style={styles.btnTexto}>Calcular</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </View>
    )
}

export default EscalaForm2;
