import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFF",
    },
    containerTitulo: {
        marginTop: 15,
        marginLeft: 40,
        marginRight: 50,
    },
    containerQuestao: {
        borderWidth: 1,
        borderBottomRightRadius: 10,
        borderTopRightRadius: 10,
        marginVertical: 15,
        marginRight: 50,
        paddingVertical: 15,
        paddingHorizontal: 45,
    },
    titulo: {
        fontFamily: 'RobotoCondensed-Bold',
        fontSize: 18,
        color: '#4E4E4E',
    },
    containerTituloQuestao: {
        marginLeft: 10,
    },
    texto1: {
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        color: '#545454',
    },
    texto2: {
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        color: '#4E4E4E',
    },

    btnOption: {
        height: 48,
        marginBottom: 25,
        backgroundColor: "#00B8AB",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 50,
    },
    textoOption: {
        fontFamily: 'Roboto-Medium',
        color: "#FFF",
        fontSize: 16
    },
    questaoCard: {
        alignItems: 'center',
        flexDirection: 'row',
    },
    questaoInput: {
    
    },
    containerBtn: {
        flex: 1,
        justifyContent: "center",
        margin: 25,
    },
    btnAction: {
        backgroundColor: "#1C72BA",
        paddingVertical: 10,
        paddingHorizontal: 30,
        marginHorizontal: 40,
        borderRadius: 25,
        justifyContent: "center",
        alignItems: "center",
    },
    btnTexto: {
        fontFamily: 'Roboto-Medium',
        fontSize: 14,
        color: "#FFF",
    },
});