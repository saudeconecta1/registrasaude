import React, { useState, useCallback } from 'react';
import { View, Text, TouchableOpacity, ScrollView, } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import { RadioButton } from 'react-native-paper';

import styles from './styles';

import Header from '../../components/HeaderEscala';

function EscalaForm() {
    const navigation = useNavigation();

    const lista = [
        {
            id: 1,
            title: 'Acamadas',
            status: false,
            error: false,
            score: 3,
            data: [
                {
                    label: 'Não',
                    value: false,
                },
                {
                    label: 'Sim',
                    value: true,
                }
            ]
        },
        {
            id: 2,
            title: 'Com deficiência física:',
            status: false,
            error: false,
            score: 3,
            data: [
                {
                    label: 'Não',
                    value: false,
                },
                {
                    label: 'Sim',
                    value: true,
                }
            ]
        },
        {
            id: 3,
            title: 'Com deficiência mental:',
            status: false,
            error: false,
            score: 3,
            data: [
                {
                    label: 'Não',
                    value: false,
                },
                {
                    label: 'Sim',
                    value: true,
                }
            ]
        },
        {
            id: 4,
            title: 'Com desnutrição grave:',
            status: false,
            error: false,
            score: 3,
            data: [
                {
                    label: 'Não',
                    value: false,
                },
                {
                    label: 'Sim',
                    value: true,
                }
            ]
        },
        {
            id: 5,
            title: 'Usuárias de drogas:',
            status: false,
            error: false,
            score: 2,
            data: [
                {
                    label: 'Não',
                    value: false,
                },
                {
                    label: 'Sim',
                    value: true,
                }
            ]
        },
        {
            id: 6,
            title: 'Desempregadas:',
            status: false,
            error: false,
            score: 2,
            data: [
                {
                    label: 'Não',
                    value: false,
                },
                {
                    label: 'Sim',
                    value: true,
                }
            ]
        },
        {
            id: 7,
            title: 'Analfabetas:',
            status: false,
            error: false,
            score: 1,
            data: [
                {
                    label: 'Não',
                    value: false,
                },
                {
                    label: 'Sim',
                    value: true,
                }
            ]
        },
        {
            id: 8,
            title: 'Com crianças com menos de 6 meses:',
            status: false,
            error: false,
            score: 1,
            data: [
                {
                    label: 'Não',
                    value: false,
                },
                {
                    label: 'Sim',
                    value: true,
                }
            ]
        },
        {
            id: 9,
            title: 'Com mais de 70 anos:',
            status: false,
            error: false,
            score: 1,
            data: [
                {
                    label: 'Não',
                    value: false,
                },
                {
                    label: 'Sim',
                    value: true,
                }
            ]
        },
        {
            id: 10,
            title: 'Hipertensão arterial sistêmica:',
            status: false,
            error: false,
            score: 1,
            data: [
                {
                    label: 'Não',
                    value: false,
                },
                {
                    label: 'Sim',
                    value: true,
                }
            ]
        },
        {
            id: 11,
            title: 'Diabetes mellitus:',
            status: false,
            error: false,
            score: 1,
            data: [
                {
                    label: 'Não',
                    value: false,
                },
                {
                    label: 'Sim',
                    value: true,
                }
            ]
        },
    ];

    const [questoes, setQuestoes] = useState(lista);
    const [erro, setErro] = useState(false);

    const checkQuestao = (questao, object) => {
        let i;
        let opcoes = object.data;

        for (i = 0; i < opcoes.length; i++) {
            if (opcoes[i].isChecked === 'checked') {
                opcoes[i].isChecked = 'unchecked';
            }
        }

        questao.isChecked = "checked";
        object.status = true;
        object.error = false;

        const index = questoes.findIndex(obj => obj.id === object.id);
        const newTodos = [...questoes];
        newTodos[index] = object;
        setQuestoes(newTodos);
    };

    const verificarQuestoes = () => {
        let total = 0;
        let count = 0;

        questoes.map((questao) => {
            if (questao.status) {
                console.log('QUESTAO: ', questao.status);
                console.log(`A questão ${questao.id} respondida!`);

                questao.data.map((item) => {

                    if (item.isChecked === 'checked' && item.value === true) {
                        total += questao.score;
                    } else {
                        total += 0;
                    }
                });
            } else {
                console.log(`A questão ${questao.id} não foi respondida!`);

                questao.error = true;

                const index = questoes.findIndex(obj => obj.id === questao.id);
                const newTodos = [...questoes];
                newTodos[index] = questao;

                setQuestoes(newTodos);

                count = 1;
            }
        });

        if (count === 0) {
            navigation.navigate('EscalaForm2', {
                total
            });
        }
    };

    return (
        <View style={styles.container}>
            <Header />
            <ScrollView
                style={styles.scrollView}
                keyboardShouldPersistTaps='always'
            >
                <View style={styles.containerTitulo}>
                    <Text style={styles.titulo}>
                        EXISTEM PESSOAS MORANDO NA CASA COM AS SEGUINTES CONDIÇÕES?
                    </Text>
                </View>

                <View style={{ flex: 1, backgroundColor: "#FFF" }}>
                    {questoes.map((object, d) =>
                        <View
                            key={d}
                            style={[
                                styles.containerQuestao,
                                object.error ?
                                    { borderColor: '#FF0000', borderWidth: 1.5 } :
                                    { borderColor: '#707070', }]}>

                            <View style={styles.containerTituloQuestao}>
                                <Text style={styles.texto1}>{object.title}</Text>
                            </View>

                            {object.data.map((questao, i) =>
                                <View key={i}>
                                    <View style={styles.questaoCard}>
                                        <RadioButton
                                            value={questao.value}
                                            status={questao.isChecked}
                                            color={'#8AC868'}
                                            onPress={() => checkQuestao(questao, object)}
                                        />
                                        <Text style={styles.texto2}>{questao.label}</Text>
                                    </View>
                                </View>
                            )}
                        </View>
                    )}
                </View>

                <View style={styles.containerBtn}>
                    <TouchableOpacity
                        style={styles.btnAction}
                        onPress={() => verificarQuestoes()}>
                        <Text style={styles.btnTexto}>Próximo</Text>
                    </TouchableOpacity>
                </View>

            </ScrollView>
        </View >
    )
}

export default EscalaForm;
