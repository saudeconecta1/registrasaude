import React, { useState, useEffect } from 'react';
import { View, Text } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import styles from './styles';

import Header from '../../components/HeaderEscala';
import TipoCaso from '../../components/TipoCasoEscala';
import FormButton from "../../components/FormButton";

export default function FeedbackEscala({ route }) {
    const navigation = useNavigation();

    const [tipoCaso, setTipoCaso] = useState(0);
    const [pontos, setPontos] = useState(0);

    useEffect(() => {
        setTipoCaso(route.params.caso);
        setPontos(route.params.pontos);
    }, [route.params.caso, route.params.pontos]);

    useEffect(() => () => console.log("unmount feedback escala"), []);

    function navigateToRegistro() {
        navigation.reset({
            routes: [{ name: 'EscalaForm' }],
        });
    }

    return (
        <View style={styles.container}>
            <Header />
            <View style={styles.containerFeedback}>
                <View style={styles.containerTexto}>
                    <TipoCaso caso={tipoCaso} />
                </View>

                <View style={styles.containerCircle}>
                    <View style={styles.circle}>
                        <Text style={styles.textoQuantidade}>{pontos}</Text>
                        <Text style={styles.textoPontos}>PONTOS</Text>
                    </View>
                </View>

                <View style={styles.containerPontos}>
                    <Text style={styles.texto}>0 a 4 pontos - Vulnerabilidade habitual</Text>
                    <Text style={styles.texto}>5 e 6 pontos - Vulnerabilidade menor</Text>
                    <Text style={styles.texto}>7 e 8 pontos - Vulnerabilidade média</Text>
                    <Text style={styles.texto}>9 pontos ou mais - Vulnerabilidade máxima</Text>
                </View>

                <View style={styles.buttonContainer}>
                    <FormButton
                        buttonType="outline"
                        onPress={navigateToRegistro}
                        title="Efetuar novo cáculo"
                        buttonColor="#1C72BA"
                    />
                </View>
            </View>
        </View>
    )
}