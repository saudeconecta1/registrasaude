import { StyleSheet } from 'react-native';
import { getStatusBarHeight } from 'react-native-status-bar-height';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFF",
    },
    containerTexto: {
        marginTop: 40,
        alignItems: "center",
    },
    containerCircle: {
        alignItems: "center",
        marginVertical: 25,
    },
    circle: {
        width: 100,
        height: 100,
        borderRadius: 100 / 2,
        backgroundColor: "#8AC868",
        justifyContent: "center",
        alignItems: "center",
        alignContent: "center",
    },
    textoQuantidade: {
        fontFamily: 'Roboto-Bold',
        fontSize: 28,
        color: "#FFF",
    },
    textoPontos: {
        fontFamily: 'Roboto-Bold',
        fontSize: 16,
        color: "#FFF",
    },
    containerPontos: {
        marginHorizontal: 30,
        marginBottom: 25,
    },
    texto: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: "#85868A",
    },
    label: {
        fontFamily: 'Roboto-Regular',
        color: "#FFF",
        fontSize: 14,
    },
    buttonContainer: {
        margin: 25
    }
});