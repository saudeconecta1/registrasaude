import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#FFF"
    },
    buttonContainer: {
        marginHorizontal: 25
    },
    containerTitle: {
        alignItems: "center",
        marginTop: 15,
        marginBottom: 10
    },
    title: {
        fontFamily: 'RobotoCondensed-Bold',
        fontSize: 18,
        textTransform: "uppercase",
        color: "#4E4E4E"
    },
    headerText: {
        fontFamily: 'Roboto-Regular',
        fontSize: 16,
        color: "#4E4E4E"
    },
    containerTermos: {
        marginTop: 10,
        marginBottom: 20,
        marginHorizontal: 50,
        alignItems: "center",
    },
    textoTermo: {
        fontFamily: 'Roboto-Regular',
        fontSize: 12,
        textAlign: "center",
        color: "#545454"
    },
    iconStyle: {
        marginRight: 10
    },
    containerInput: {
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: '#545454',
        borderRadius: 25,
        alignItems: 'center',
        marginHorizontal: 50,
        paddingLeft: 20,
        alignItems: 'center',
        flexDirection: 'row',
    },
    input: {
        flex: 1,
        marginLeft: 10,
        fontFamily: 'Roboto-Regular',
        fontSize: 14,
        color: "#545454",
    },
});