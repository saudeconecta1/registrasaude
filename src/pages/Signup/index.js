import React, { useState, useRef } from "react";
import {
    Alert,
    Platform,
    View,
    Text,
    TouchableOpacity,
    KeyboardAvoidingView,
    ScrollView,
    Keyboard
} from "react-native";

import { useNavigation } from '@react-navigation/native';
import { Button } from "react-native-elements";
import { TextInputMask } from 'react-native-masked-text';

import Ionicons from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { Formik } from "formik";
import * as Yup from "yup";

import Header from '../../components/HeaderIntro';
import FormInput from "../../components/FormInput";
import FormButton from "../../components/FormButton";
import ErrorMessage from "../../components/ErrorMessage";
import { AuthService } from '../../api';
import AsyncStorage from '@react-native-community/async-storage';

import styles from './styles';

const validationSchema = Yup.object().shape({
    cpf: Yup.string()
        .label("CPF")
        .required("Digite seu CPF"),
    email: Yup.string()
        .label("Email")
        .email("Entre com um email válido")
        .required("Digite um e-mail"),
    password: Yup.string()
        .label("Password")
        .required("Digite uma senha")
        .min(8, "A senha deve ter pelo menos 8 caracteres"),
    password_confirmation: Yup.string()
        .oneOf([Yup.ref("password")], "Confirme se a senha deve corresponder à senha")
        .required("Confirmar senha é obrigatório")
});

function Signup() {
    const authService = new AuthService();
    const navigation = useNavigation();

    const [passwordVisibility, setPasswordVisibility] = useState(true);
    const [passwordIcon, setPasswordIcon] = useState("ios-eye");
    const [confirmPasswordIcon, setConfirmPasswordIcon] = useState("ios-eye");
    const [confirmPasswordVisibility, setConfirmPasswordVisibility] = useState(
        true
    );

    const inputRef2 = useRef(null);
    const inputRef3 = useRef(null);
    const inputRef4 = useRef(null);

    function goToLogin() {
        navigation.navigate("Login");
    }

    function handlePasswordVisibility() {
        if (passwordIcon === "ios-eye") {
            setPasswordIcon("ios-eye-off");
            setPasswordVisibility(!passwordVisibility);
        } else if (passwordIcon === "ios-eye-off") {
            setPasswordIcon("ios-eye");
            setPasswordVisibility(!passwordVisibility);
        }
    }

    function handleConfirmPasswordVisibility() {
        if (confirmPasswordIcon === "ios-eye") {
            setConfirmPasswordIcon("ios-eye-off");
            setConfirmPasswordVisibility(!confirmPasswordVisibility);
        } else if (confirmPasswordIcon === "ios-eye-off") {
            setConfirmPasswordIcon("ios-eye");
            setConfirmPasswordVisibility(!confirmPasswordVisibility);
        }
    }

    function buildForm(values) {
        const formData = new FormData();
        values.cpf = values.cpf.replace(/[^\d]+/g, '');

        Object.keys(values).forEach(key => {
            formData.append(key, values[key]);
        });
        formData.append("name", 'app_cadastro')
        formData.append("device_name", 'mobile')
        return formData;
    }

    async function saveUser(resultUser) {
        const { user } = resultUser;
        await AsyncStorage.setItem('@ListApp:userToken', JSON.stringify(resultUser))
        const objAviso = { idUser: user.id, aviso: true };
        await AsyncStorage.setItem('@ListApp:aviso', JSON.stringify(objAviso))
    }

    async function handleOnSignup(values, actions) {
        const form = buildForm(values);

        try {
            const result = await authService.cadastrar(form);
            await saveUser(result.data);

            actions.setSubmitting(false);

            navigation.reset({
                index: 0,
                routes: [{ name: 'App' }],
            });

            Keyboard.dismiss();

        } catch (e) {
            const data = e.response.data;
            const msg = Object.keys(data.errors).map(k => {
                return data.errors[k].join(', ')
            }).join(', ');
            console.log('erro ao cadastrar', e.response.data);
            Alert.alert('Aviso', msg,
                [{ text: 'OK' }],
                { cancelable: false },
            );
            actions.setSubmitting(false);
        }
    }

    return (
        <KeyboardAvoidingView
            style={styles.container}
            enabled
            behavior={Platform.OS === "ios" ? "padding" : null}>
            <ScrollView>
                <Header />

                <View style={styles.containerTitle}>
                    <Text style={styles.title}>Você é um novo usuário</Text>
                    <Text style={styles.headerText}>Faça seu registro:</Text>
                </View>

                <Formik
                    initialValues={{
                        email: "",
                        cpf: "",
                        password: "",
                        password_confirmation: ""
                    }}
                    onSubmit={(values, actions) => {
                        handleOnSignup(values, actions);
                    }}
                    validationSchema={validationSchema}
                >
                    {({
                        handleChange,
                        values,
                        handleSubmit,
                        errors,
                        isValid,
                        touched,
                        handleBlur,
                        isSubmitting
                    }) => (
                            <>
                                <View style={styles.containerInput}>
                                    <Icon
                                        name={'account-outline'}
                                        size={22}
                                        color="#545454" />
                                    <TextInputMask
                                        style={styles.input}
                                        name="cpf"
                                        type={'cpf'}
                                        value={values.cpf}
                                        onChangeText={handleChange("cpf")}
                                        onBlur={handleBlur("cpf")}
                                        placeholder="CPF"
                                        placeholderTextColor="#545454"
                                        underlineColorAndroid="transparent"
                                        returnKeyType={"next"}
                                        onSubmitEditing={() => { inputRef2.current.focus(); }}
                                        blurOnSubmit={false}
                                    />
                                </View>
                                <ErrorMessage errorValue={touched.cpf && errors.cpf} />

                                <FormInput
                                    name="email"
                                    minhaRef={inputRef2}
                                    value={values.email}
                                    keyboardType="email-address"
                                    onChangeText={handleChange("email")}
                                    placeholder="E-mail"
                                    autoCapitalize="none"
                                    autoCorrect={false}
                                    iconName="email-outline"
                                    iconColor="#545454"
                                    onBlur={handleBlur("email")}
                                    returnKeyType={"next"}
                                    onSubmitEditing={() => { inputRef3.current.focus(); }}
                                    blurOnSubmit={false}
                                />
                                <ErrorMessage errorValue={touched.email && errors.email} />

                                <FormInput
                                    name="password"
                                    minhaRef={inputRef3}
                                    value={values.password}
                                    onChangeText={handleChange("password")}
                                    placeholder="Senha"
                                    iconName="lock-outline"
                                    iconColor="#545454"
                                    onBlur={handleBlur("password")}
                                    secureTextEntry={passwordVisibility}
                                    rightIcon={
                                        <TouchableOpacity onPress={handlePasswordVisibility}>
                                            <Ionicons
                                                style={styles.iconStyle}
                                                name={passwordIcon}
                                                size={28}
                                                color="grey" />
                                        </TouchableOpacity>
                                    }
                                    returnKeyType="next"
                                    onSubmitEditing={() => { inputRef4.current.focus(); }}
                                    blurOnSubmit={false}
                                />
                                <ErrorMessage errorValue={touched.password && errors.password} />

                                <FormInput
                                    name="password_confirmation"
                                    minhaRef={inputRef4}
                                    value={values.password_confirmation}
                                    onChangeText={handleChange("password_confirmation")}
                                    placeholder="Confirmar senha"
                                    iconName="lock-outline"
                                    iconColor="#545454"
                                    onBlur={handleBlur("password_confirmation")}
                                    secureTextEntry={confirmPasswordVisibility}
                                    rightIcon={
                                        <TouchableOpacity onPress={handleConfirmPasswordVisibility}>
                                            <Ionicons
                                                style={styles.iconStyle}
                                                name={confirmPasswordIcon}
                                                size={28}
                                                color="grey"
                                            />
                                        </TouchableOpacity>
                                    }
                                    returnKeyType="send"
                                />
                                <ErrorMessage
                                    errorValue={touched.password_confirmation && errors.password_confirmation}
                                />

                                <View style={styles.containerTermos}>
                                    <Text style={styles.textoTermo}>
                                        Ao se registrar, você concorda com os
                                    </Text>
                                    <Text style={styles.textoTermo}>
                                        TERMOS DE USO do aplicativo
                                    </Text>
                                </View>

                                <View style={styles.buttonContainer}>
                                    <FormButton
                                        buttonType="outline"
                                        onPress={handleSubmit}
                                        title="Registrar"
                                        buttonColor="#1C72BA"
                                        disabled={!isValid || isSubmitting}
                                        loading={isSubmitting}
                                    />
                                </View>
                                <ErrorMessage errorValue={errors.general} />
                            </>
                        )}
                </Formik>

                <Button
                    title="Voltar para Login"
                    onPress={goToLogin}
                    titleStyle={{
                        fontFamily: 'Roboto-Regular',
                        fontSize: 12,
                        color: "#545454"
                    }}
                    type="clear"
                />
            </ScrollView>
        </KeyboardAvoidingView>
    );
}

export default Signup;
