//import SQLite from 'react-native-sqlite-storage';
let SQLite = require('react-native-sqlite-storage');

//SQLite.DEBUG(true);
SQLite.enablePromise(true);

const config = {
    name: 'covid.db',
    createFromLocation: '1.0.0',
    location: 'default'
};
const database_name = "covid.db";
const database_version = "1.0";
const database_displayname = "COVID Database";
const database_size = 200000;

export default class Database {
    initDB() {
        let db;
        return new Promise((resolve) => {
            console.log("Plugin integrity check ...");
            SQLite.echoTest()
                .then(() => {
                    console.log("Integrity check passed ...");
                    console.log("Opening database ...");
                    SQLite.openDatabase(
                        database_name,
                        database_version,
                        database_displayname,
                        database_size
                    )
                        .then(DB => {
                            db = DB;
                            console.log("Database OPEN");

                            //db.executeSql('DROP TABLE Relatorio');
                            //db.executeSql('DROP TABLE Nota');
                            //db.executeSql('DROP TABLE Sincronia');

                            //TABELA RELATORIO
                            db.executeSql('SELECT 1 FROM Relatorio LIMIT 1').then(() => {
                                console.log("Database is ready ... executing query ... Check Relatorio");
                            }).catch((error) => {
                                console.log("Received error: ", error);
                                console.log("Database not yet ready ... populating data");
                                db.transaction((tx) => {
                                    tx.executeSql(
                                        'CREATE TABLE IF NOT EXISTS Relatorio (id INTEGER PRIMARY KEY AUTOINCREMENT, idUser INTEGER, json TEXT, tipoCaso INT, status INT)'
                                    );
                                }).then(() => {
                                    console.log("Table created successfully");
                                }).catch(error => {
                                    console.log(error);
                                });
                            });

                            //TABELA NOTA
                            db.executeSql('SELECT 1 FROM Nota LIMIT 1').then(() => {
                                console.log('Database is ready ... executing query ... Check Nota');
                            }).catch(error => {
                                console.log('Received error: ', error);
                                console.log('Database not yet ready ... populating data');
                                db.transaction(tx => {
                                    tx.executeSql(
                                        'CREATE TABLE IF NOT EXISTS Nota (id INTEGER PRIMARY KEY AUTOINCREMENT, idUser INTEGER, anotacao TEXT, favorito INTEGER, transactionDate TEXT, status INT)',
                                    );
                                }).then(() => {
                                    console.log('Table created successfully');
                                }).catch(error => {
                                    console.log(error);
                                });
                            });

                            //TABELA SINCRONIA
                            db.executeSql('SELECT 1 FROM Sincronia LIMIT 1').then(() => {
                                console.log('Database is ready ... executing query ... Check Sincronia');
                            }).catch(error => {
                                console.log('Received error: ', error);
                                console.log('Database not yet ready ... populating data');
                                db.transaction(tx => {
                                    tx.executeSql(
                                        'CREATE TABLE IF NOT EXISTS Sincronia (id INTEGER PRIMARY KEY AUTOINCREMENT, idUser, sincroniaDate TEXT NOT NULL)',
                                    );
                                }).then(() => {
                                    console.log('Table created successfully');
                                }).catch(error => {
                                    console.log(error);
                                });
                            });

                            resolve(db);
                        })
                        .catch(error => {
                            console.log(error);
                        });
                }).catch(error => {
                    console.log("echoTest failed - plugin not functional");
                });
        });
    }

    errorCB(err) {
        console.log("SQL Error: " + err);
    }

    closeDatabase(db) {
        if (db) {
            db.close((err) => {
                if (err) {
                    return console.error("ERORR: ", err.message);
                }
                console.log('Close the database connection.');
            });

        } else {
            console.log("Database was not OPENED");
        }
    }

    listarRelatorioNaoSincronizados(idUser) {
        return new Promise((resolve) => {
            const relatorios = [];
            this.initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('SELECT * FROM Relatorio WHERE status = 0 AND idUser = ?', [idUser]).then(([tx, results]) => {
                        var len = results.rows.length;
                        for (let i = 0; i < len; i++) {
                            let row = results.rows.item(i);
                            const { id, idUser, json, tipoCaso, status } = row;
                            relatorios.push({ id, idUser, relatorio: JSON.parse(json), tipoCaso, status });
                        }
                        resolve(relatorios);
                    });
                }).then((result) => {
                    console.log('Close DB: listarRelatorioNaoSincronizados')
                    //this.closeDatabase(db);
                }).catch((err) => {
                    console.log(err);
                });
            }).catch((err) => {
                console.log(err);
            });
        });
    }

    adicionarRelatorio(relatorio) {
        return new Promise((resolve) => {
            this.initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('INSERT INTO Relatorio(idUser, json, tipoCaso, status) VALUES (?, ?, ?, ?)',
                        [
                            relatorio.idUser,
                            relatorio.json,
                            relatorio.tipoCaso,
                            relatorio.status
                        ]).then(([tx, results]) => {
                            resolve(results);
                        });
                }).then((result) => {
                    console.log('Close DB: adicionarRelatorio')
                    //this.closeDatabase(db);
                }).catch((err) => {
                    console.log(err);
                });
            }).catch((err) => {
                console.log(err);
            });
        });
    }

    /* NOTAS DO USUARIO LOGADO */
    getQuantidadeCasosSuspeitos(idUser) {
        return new Promise((resolve) => {
            this.initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql(
                        'SELECT count(id) as qtd FROM Relatorio WHERE tipoCaso = 1 AND idUser = ?',
                        [idUser]
                    ).then(([tx, res]) => {
                        var qtd = res.rows.length;
                        console.log("CASOS SUSPEITOS: " + res.rows.item(0).qtd);
                        resolve({ "qtd": res.rows.item(0).qtd });
                    });
                }).then((result) => {
                    console.log('Close DB: getQuantidadeNotas')
                    //this.closeDatabase(db);
                }).catch((err) => {
                    console.log(err);
                });
            }).catch((err) => {
                console.log(err);
            });
        });
    }

    //Insere a data da ultima sincronização feita pelo usuario
    adicionarSincronia(idUser, sincroniaDate) {
        return new Promise((resolve) => {
            this.initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('INSERT INTO Sincronia(idUser, sincroniaDate) VALUES (?, ?)',
                        [idUser, sincroniaDate]).then(([tx, results]) => {
                            resolve(results);
                        });
                }).then((result) => {
                    console.log('Close DB: adicionarSincronia')
                    this.closeDatabase(db);
                }).catch((err) => {
                    console.log(err);
                });
            }).catch((err) => {
                console.log(err);
            });
        });
    }

    deleteSincronia(id) {
        return new Promise((resolve) => {
            this.initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('DELETE FROM Sincronia WHERE id = ?', [id])
                        .then(([tx, results]) => {
                            console.log(results);
                            resolve(results);
                        });
                }).then((result) => {
                    console.log('Close DB: deleteSincronia')
                    //this.closeDatabase(db);
                }).catch((err) => {
                    console.log(err);
                });
            }).catch((err) => {
                console.log(err);
            });
        });
    }

    /* REGISTROS NAO SINCRONIZADOS DO USUARIO LOGADO */
    getQtdRegistros(idUser) {
        return new Promise((resolve) => {
            this.initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql(
                        'SELECT COUNT(id) as qtd FROM Relatorio WHERE idUser = ? AND status = 0',
                        [idUser]
                    ).then(([tx, res]) => {
                        var qtd = res.rows.length;
                        console.log("::REGISTROS NÃO SINCRONIZADOS: " + res.rows.item(0).qtd);
                        resolve(res.rows.item(0).qtd);
                    });
                }).then((result) => {
                    console.log('Close DB: getQuantidadeNotas')
                    this.closeDatabase(db);
                }).catch((err) => {
                    console.log(err);
                });
            }).catch((err) => {
                console.log(err);
            });
        });
    }

    //Recupera a data da ultima sincronização feita pelo usuario
    getDateSincronia(idUser) {
        console.log("USUARIO LOGADO", idUser);
        return new Promise((resolve) => {
            this.initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql(
                        'SELECT s.sincroniaDate, (SELECT COUNT(r.id) FROM Relatorio r WHERE r.idUser = s.idUser AND r.status = 0) as qtd FROM Sincronia s WHERE s.idUser = ? ORDER BY s.id DESC LIMIT 1;',
                        [idUser]
                    ).then(([tx, results]) => {
                        var len = results.rows.length;
                        console.log('::NUMERO DE REGISTROS: ', len);
                        if (len > 0) {
                            console.log("::RESULTADO DADOS SINCRONIA: ", results.rows.item(0))
                            resolve(results.rows.item(0));
                        } else {
                            console.log("::SEM RESULTADOS DE DADOS SINCRONIA")
                            resolve(null);
                        }
                    });
                }).then((result) => {
                    console.log('Close DB: getDateSincronia')
                    //this.closeDatabase(db);
                }).catch((err) => {
                    console.log(err);
                });
            }).catch((err) => {
                console.log(err);
            });
        });
    }

    /* NOTAS DO USUARIO LOGADO */
    getQuantidadeNotas(idUser) {
        return new Promise((resolve) => {
            this.initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql(
                        'SELECT * FROM NOTA WHERE idUser = ?',
                        [idUser]
                    ).then(([tx, results]) => {
                        var qtd = results.rows.length;
                        resolve({ "qtd": qtd });
                    });
                }).then((result) => {
                    console.log('Close DB: getQuantidadeNotas')
                    this.closeDatabase(db);
                }).catch((err) => {
                    console.log(err);
                });
            }).catch((err) => {
                console.log(err);
            });
        });
    }

    listarNotas(idUser) {
        return new Promise((resolve) => {
            const notas = [];
            this.initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql(
                        'SELECT * FROM Nota WHERE idUser = ? ORDER BY favorito desc, datetime(transactionDate) DESC;',
                        [idUser]).then(([tx, results]) => {
                            console.log("Query completed");
                            var len = results.rows.length;
                            for (let i = 0; i < len; i++) {
                                let row = results.rows.item(i);
                                console.log(`Nota ID: ${row.id}`);
                                const { id, idUser, anotacao, favorito, transactionDate, status } = row;

                                notas.push({
                                    id,
                                    idUser,
                                    anotacao,
                                    favorito,
                                    transactionDate,
                                    status
                                });
                            }
                            resolve(notas);
                        });
                }).then((result) => {
                    console.log('Close DB: listarNotas')
                    //this.closeDatabase(db);
                }).catch((err) => {
                    console.log(err);
                });
            }).catch((err) => {
                console.log(err);
            });
        });
    }

    adicionarNota(notas) {
        return new Promise((resolve) => {
            this.initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('INSERT INTO Nota(idUser, anotacao, favorito, transactionDate, status) VALUES (?, ?, ?, ?, ?)',
                        [
                            notas.idUser,
                            notas.anotacao,
                            notas.favorito,
                            notas.transactionDate,
                            notas.status
                        ], (_, result) => {
                            resolve(result);
                        },
                        (_, err) => {
                            reject(err);
                            console.log(err.message, 'err');
                        },
                    );
                });
            });
        });
    }

    updateNota(nota) {
        return new Promise((resolve) => {
            this.initDB().then((db) => {
                const arrData = [nota.anotacao, nota.id];
                db.transaction((tx) => {
                    tx.executeSql('UPDATE Nota SET anotacao = ? WHERE id = ?', arrData)
                        .then(([tx, results]) => {
                            resolve(results);
                        });
                }).then((result) => {
                    console.log('Close DB: updateNota')
                    //this.closeDatabase(db);
                }).catch((err) => {
                    console.log(err);
                });
            }).catch((err) => {
                console.log(err);
            });
        });
    }

    favoritarNota(id, favorito) {
        return new Promise((resolve) => {
            this.initDB().then((db) => {
                const arrData = [favorito, id];
                db.transaction((tx) => {
                    tx.executeSql(
                        `UPDATE Nota SET favorito = ? WHERE id = ?`, arrData
                    ).then(([tx, results]) => {
                        resolve(results);
                    });
                }).then((result) => {
                    console.log('Close DB: favoritarNota')
                    //this.closeDatabase(db);
                }).catch((err) => {
                    console.log(err);
                });
            }).catch((err) => {
                console.log(err);
            });
        });
    }

    deleteNota(id) {
        return new Promise((resolve) => {
            this.initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql('DELETE FROM Nota WHERE id = ?', [id])
                        .then(([tx, results]) => {
                            console.log(results);
                            resolve(results);
                        });
                }).then((result) => {
                    console.log('Close DB: deleteNota')
                    //this.closeDatabase(db);
                }).catch((err) => {
                    console.log(err);
                });
            }).catch((err) => {
                console.log(err);
            });
        });
    }

    atualizarSincronizados(id, idUser) {
        return new Promise((resolve) => {
            this.initDB().then((db) => {
                db.transaction((tx) => {
                    tx.executeSql(`UPDATE Relatorio SET status = 1 WHERE id IN (${id.join(', ')}) AND idUser = ?;`,
                        [idUser]).then(([tx, results]) => {
                            resolve(results);
                        });
                }).then((result) => {
                    console.log('Close DB: atualizarSincronizados')
                    //this.closeDatabase(db);
                }).catch((err) => {
                    console.log(err);
                });
            }).catch((err) => {
                console.log(err);
            });
        });
    }
}