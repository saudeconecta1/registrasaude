import React, { useState, useEffect, useContext, useMemo } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import FontAwesome from 'react-native-vector-icons/FontAwesome';

import {
  SignIn,
  CreateAccount,
  Search,
  Home,
  Details,
  Search2,
  Profile,
  Splash
} from "./Screens";

import Login from './pages/Login';
import RegistroForm from './pages/RegistroForm';
import Notas from './pages/Notas';
import Sincronizacao from './pages/Sincronizacao';
import Config from './pages/Config';
import Feedback from './pages/Feedback';

const AuthStack = createStackNavigator();
const AuthStackScreen = () => (
  <AuthStack.Navigator>
    <AuthStack.Screen
      name="Login"
      component={Login}
      options={{ headerShown: false }}
    />
    <AuthStack.Screen
      name="CreateAccount"
      component={CreateAccount}
      options={{ headerShown: false }}
    />
  </AuthStack.Navigator>
);

const Tabs = createBottomTabNavigator();
const TabsScreen = () => (
  <Tabs.Navigator
    screenOptions={({ route }) => ({
      tabBarIcon: ({ focused, color, size }) => {
        let iconName;

        if (route.name === 'Registro') {
          iconName = focused ? 'home' : 'home';
        } else if (route.name === 'Notas') {
          iconName = focused ? 'edit' : 'edit';
        } else if (route.name === 'Sincronizacao') {
          iconName = focused ? 'refresh' : 'refresh';
        } else if (route.name === 'Config') {
          iconName = focused ? 'gear' : 'gear';
        }

        // You can return any component that you like here!
        return <FontAwesome name={iconName} size={size} color={color} />;
      }
    })}
    tabBarOptions={{
      activeTintColor: '#00B8AB',
      inactiveTintColor: '#4E4E4E',
      showLabel: false,
    }}
  >
    <Tabs.Screen
      name="Registro"
      component={RegistroForm}
      listeners={({ navigation, route }) => ({
        tabPress: e => {
          e.preventDefault();
          navigation.reset({ routes: [{ name: 'Registro' }] });
        },
      })} />
    <Tabs.Screen name="Notas" component={Notas} />
    <Tabs.Screen name="Sincronizacao" component={Sincronizacao} />
    <Tabs.Screen name="Config" component={Config} />
  </Tabs.Navigator>
);

const RootStack = createStackNavigator();
const RootStackScreen = () => (
  <RootStack.Navigator headerMode="none">
    <RootStack.Screen
      name="App"
      component={TabsScreen}
      options={{
        animationEnabled: false
      }}
    />
    <RootStack.Screen
      name="Feedback"
      component={Feedback}
      options={{
        animationEnabled: false
      }}
    />
    <RootStack.Screen
      name="Auth"
      component={AuthStackScreen}
      options={{
        animationEnabled: false
      }}
    />
  </RootStack.Navigator>
);

export default () => {
  return (
    <NavigationContainer>
      <RootStackScreen />
    </NavigationContainer>
  );
};