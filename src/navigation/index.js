import 'react-native-gesture-handler';

import React, { useEffect } from "react";
import SplashScreen from 'react-native-splash-screen';

import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import AuthLoadingScreen from './AuthLoadingScreen';
import AuthNavigation from './AuthNavigation';
import AppNavigation from './AppNavigation';
import Onboarding from '../pages/Onboarding';
import CovidAppContext from '../contexts/CovidAppContext';

import Database from '../banco/Database';

const RootStack = createStackNavigator();

function RootStackScreen() {
  return (
    <RootStack.Navigator
      screenOptions={{
        cardStyle: { backgroundColor: 'transparent' },
        cardOverlayEnabled: true,
        cardStyleInterpolator: ({ current: { progress } }) => ({
          cardStyle: {
            opacity: progress.interpolate({
              inputRange: [0, 0.5, 0.9, 1],
              outputRange: [0, 0.25, 0.7, 1],
            }),
          },
          overlayStyle: {
            opacity: progress.interpolate({
              inputRange: [0, 1],
              outputRange: [0, 0.5],
              extrapolate: 'clamp',
            }),
          },
        }),
      }}
      mode="modal"
      headerMode="none">

      <RootStack.Screen
        name="AuthLoading"
        component={AuthLoadingScreen}
        options={{
          animationEnabled: false
        }}
      />
      <RootStack.Screen
        name="Auth"
        component={AuthNavigation}
        options={{
          animationEnabled: false
        }}
      />
      <RootStack.Screen
        name="App"
        component={AppNavigation}
        options={{
          animationEnabled: false
        }}
      />
      <RootStack.Screen
        name="Onboarding"
        component={Onboarding}
      />
    </RootStack.Navigator>
  );
}

export default function AppContainer() {
  const db = new Database();
  db.initDB();
  
  const [hasNotaContext, setHasNotaContext] = React.useState(false);
  const [hasSyncContext, setHasSyncContext] = React.useState(false);
  const [hasAvisoContext, setHasAvisoContext] = React.useState(false);
  useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <CovidAppContext.Provider
      value={
        {
          hasNotaContext,
          setHasNotaContext,
          hasSyncContext,
          setHasSyncContext,
          hasAvisoContext,
          setHasAvisoContext
        }
      }>
      <NavigationContainer>
        <RootStackScreen />
      </NavigationContainer>
    </CovidAppContext.Provider>
  );
};