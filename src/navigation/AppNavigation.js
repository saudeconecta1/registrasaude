import React, { useState, useEffect, useCallback } from 'react';
import { View, Text, BackHandler } from 'react-native';

import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createStackNavigator } from "@react-navigation/stack";

import { useNavigation } from '@react-navigation/native';

import { getUser, getAviso, salvarAviso } from '../../src/utils';
import Database from '../banco/Database';

import Home from '../pages/Home';

import RegistroForm from '../pages/RegistroForm';
import EscalaForm from '../pages/EscalaForm';
import EscalaForm2 from '../pages/EscalaForm2';
import FeedbackEscala from '../pages/FeedbackEscala';

import Notas from '../pages/Notas';
import Sincronizacao from '../pages/Sincronizacao';
import Config from '../pages/Config';
import Feedback from "../pages/Feedback";

import Probabilidade from "../pages/Probabilidade";

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import CovidAppContext from '../contexts/CovidAppContext';

const Tabs = createBottomTabNavigator();
const RegistroStack = createStackNavigator();
const SincStack = createStackNavigator();

const RegistroStackScreen = () => (
  <RegistroStack.Navigator
    initialRouteName="Home"
    headerMode="none">
    <RegistroStack.Screen
      name="Home"
      component={Home}
      options={{
        animationEnabled: false
      }}
    />
    <RegistroStack.Screen
      name="RegistroForm"
      component={RegistroForm}
      options={{ headerShown: false }}
    />
    <RegistroStack.Screen
      name="Feedback"
      component={Feedback}
      options={{ headerShown: false }}
    />
    <RegistroStack.Screen
      name="EscalaForm"
      component={EscalaForm}
      options={{ headerShown: false }}
    />
    <RegistroStack.Screen
      name="EscalaForm2"
      component={EscalaForm2}
      options={{ headerShown: false }}
    />
    <RegistroStack.Screen
      name="FeedbackEscala"
      component={FeedbackEscala}
      options={{ headerShown: false }}
    />
  </RegistroStack.Navigator>
);

const SincStackScreen = () => (
  <SincStack.Navigator
    initialRouteName="Sincronizacao"
    headerMode="none">
    <SincStack.Screen
      name="Sincronizacao"
      component={Sincronizacao}
      options={{
        animationEnabled: false
      }}
    />
    <SincStack.Screen
      name="Probabilidade"
      component={Probabilidade}
      options={{ headerShown: false }}
    />
  </SincStack.Navigator>
);

function AppNavigation() {
  const db = new Database();
  const navigation = useNavigation();
  const [userLogado, setUserLogado] = useState('');
  const { hasNotaContext, setHasNotaContext } = React.useContext(CovidAppContext);
  const { hasSyncContext, setHasSyncContext } = React.useContext(CovidAppContext);
  const { hasAvisoContext, setHasAvisoContext } = React.useContext(CovidAppContext);

  //Onboarding OVERLAY!!!
  function showOnboarding() {
    navigation.navigate('Onboarding');
  }

  if (hasAvisoContext) {
    showOnboarding();
  }

  useEffect(() => {
    loadData();
  }, []);

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handlerBackPress);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handlerBackPress);
    };
  }, [handlerBackPress]);

  const handlerBackPress = () => {
    return true;
  }

  const loadData = useCallback(async () => {
    try {
      const dataUser = await getUser();
      const { user } = JSON.parse(dataUser);
      setUserLogado(user);

      //Forçar remoção do storage
      //await AsyncStorage.removeItem('@ListApp:aviso')

      let aviso = await getAviso();

      //caso tenha sido removido ou não exista, cria ou recria
      if (!aviso) {
        await salvarAviso({ idUser: user.id, aviso: true });
        aviso = await getAviso();
      }

      const mostrarAviso = JSON.parse(aviso);

      console.log("TEM AVISO SALVO NO ASYNC:", mostrarAviso);
      console.log("AVISO EXIBIR: ", mostrarAviso.aviso);
      console.log("AVISO ID USER: ", mostrarAviso.idUser);
      console.log("USER LOGADO: ", user.id);

      if (user.id === mostrarAviso.idUser) {
        console.log("Verifica se pode qual Status do Aviso: ", mostrarAviso.aviso);
        setHasAvisoContext(mostrarAviso.aviso);
      }

      const data = await db.getQuantidadeNotas(user.id);
      console.log("QUANTIDADE DE NOTAS", data.qtd, data);

      const temNotas = data.qtd > 0;
      setHasNotaContext(temNotas);

      const dataSync = await db.getDateSincronia(user.id);

      if (dataSync) {
        console.log("::QUANTIDADE DE REGISTROS PARA SINCRONIZAR", dataSync.qtd, dataSync);

        const temRegistroSincronia = dataSync.qtd > 0;
        setHasSyncContext(temRegistroSincronia);
      } else {
        const qtdRegNaoSinc = await db.getQtdRegistros(user.id);
        console.log("::QUANTIDADE DE REGISTROS NÃO SINCRONIZADOS:", qtdRegNaoSinc);

        setHasSyncContext(qtdRegNaoSinc > 0);
      }

    } catch (err) {
      console.log(err);
    }
  });

  function IconWithBadge({ name, badgeCount, color, size }) {
    return (
      <View style={{ width: 24, height: 24, margin: 5 }}>
        <Icon name={name} size={size} color={color} />
        {badgeCount > 0 && (
          <View
            style={{
              position: 'absolute',
              right: -6,
              top: -3,
              backgroundColor: 'red',
              borderRadius: 5,
              width: 10,
              height: 10,
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Text style={{ color: 'white', fontSize: 10, fontWeight: 'bold' }}>

            </Text>
          </View>
        )}
      </View>
    );
  }

  return (
    <Tabs.Navigator
      screenOptions={({ route }) => ({
        tabBarIcon: ({ focused, color, size }) => {
          let iconName;
          let isBadge;

          if (route.name === 'Home') {
            iconName = focused ? 'home' : 'home';
            isBadge = false;
          } else if (route.name === 'Notas') {
            isBadge = hasNotaContext;
            iconName = focused ? 'file-document-edit' : 'file-document-edit';
          } else if (route.name === 'Sincronizacao') {
            isBadge = hasSyncContext;
            iconName = focused ? 'sync' : 'sync';
          } else if (route.name === 'Config') {
            isBadge = false;
            iconName = focused ? 'settings' : 'settings';
          }

          return <IconWithBadge
            name={iconName}
            size={size}
            color={color}
            badgeCount={isBadge}
          />;
        }
      })}
      tabBarOptions={{
        activeTintColor: '#00B8AB',
        inactiveTintColor: '#4E4E4E',
        showLabel: false,
      }}
    >
      <Tabs.Screen
        name="Home"
        component={RegistroStackScreen}
        listeners={({ navigation }) => ({
          tabPress: e => {
            //e.preventDefault();
            navigation.reset({ routes: [{ name: 'Home' }] });
          },
        })}
      />
      <Tabs.Screen name="Notas" component={Notas} />
      <Tabs.Screen
        name="Sincronizacao"
        component={SincStackScreen}
        listeners={({ navigation }) => ({
          tabPress: e => {
            //e.preventDefault();
            navigation.reset({ routes: [{ name: 'Sincronizacao' }] });
          },
        })}
      />
      <Tabs.Screen name="Config" component={Config} />
    </Tabs.Navigator>
  )
}

export default AppNavigation;
