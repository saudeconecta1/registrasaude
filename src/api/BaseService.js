import api from '../services/api';

export default class BaseService {
    get api(){
        return api;
    }
}