import BaseService from './BaseService';

export default class RelatorioService extends BaseService {

    urlRelatorio = '/relatorio';

    async enviarRelatorio(relatorio) {
        const headers = { 'Content-Type' : 'multipart/form-data' };
        return this.api.post(this.urlRelatorio, relatorio, {
            headers
        });
    }
}