import BaseService from './BaseService';

export default class AuthService extends BaseService {

    urlToken = 'sanctum/token';
    urlCadastro = 'sanctum/create';
    urlResetPassword = 'password/reset';

    token(form) {
        const url = new URL(this.urlToken, this.endpoint);
        let headers = this.getDefaultHeaders();
        headers.append('Content-Type', 'multipart/form-data');
        let config = { method: 'POST', headers, body: form };

        return new Promise((resolver, reject) => {
            fetch(url, config).then(response => {
                resolver(response);
            }).catch(err => {
                reject(err);
            });
        });
    }

    async cadastrar(form) {
        const headers = { 'Content-Type': 'multipart/form-data' };
        return this.api.post(this.urlCadastro, form, {
            headers
        });
    }

    async esqueceuSenha(form) {
        const headers = { 'Content-Type': 'application/x-www-form-urlencoded' };
        return this.api.post(this.urlResetPassword, form, {
            headers
        });
    }
}
